![Build](https://github.com/ThePythonator/Super-Square-Bros/workflows/Build/badge.svg)

# Super Square Bros.

A platformer for 32blit.

[itch.io](https://scorpion-games-uk.itch.io/super-square-bros)

## Controls

Left/Right to Walk, A to Jump
Up/Down to navigate menus, A to select, Y to go back/pause.

If using keyboard:
WASD/arrow keys to walk and navigate menus
U to jump/select, P to go back/pause 

## Dependencies

If building from source, it requires requires sdl2, sdl2-image and sdl2-net (Windows executables are already packaged with the necessary dlls)

### Fedora dependency installation

```bash
sudo dnf install \
git \
gcc g++ \
cmake \
make \
python3 python3-pip python3-setuptools \
unzip

cd ..  # considering being in Super-Square-Bros/ directory
git clone https://github.com/32blit/32blit-sdk
git clone https://github.com/raspberrypi/pico-sdk --recursive
git clone https://github.com/raspberrypi/pico-extras --recursive
cd Super-Square-Bros/

python3 -m pip install 32blit
export PATH=$PATH:~/.local/bin
```
For further system specific requirements see down below.

## Building

### PicoSystem

#### System Specific Requirements

Fedora:
```bash
sudo dnf install -y \
arm-none-eabi-gcc-cs arm-none-eabi-gcc-cs-c++ arm-none-eabi-newlib
```
Ubuntu:
```bash
sudo apt install gcc-arm-none-eabi
```

#### Actual building
```bash
export PATH=$PATH:~/.local/bin
mkdir -p build.pico
cd build.pico
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../32blit-sdk/pico.toolchain -DPICO_BOARD=pimoroni_picosystem
make
```

### Linux

#### System Specific Requirements

Fedora:
```bash
sudo dnf install -y SDL2-devel SDL2_image-devel SDL2_net-devel
```
Ubuntu:
```bash
sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-net-dev
```

#### Actual building:
```bash
export PATH=$PATH:~/.local/bin
mkdir -p build
cd build
cmake ..
make
```
