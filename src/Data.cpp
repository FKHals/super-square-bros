#include "Data.hpp"

#include "engine/save.hpp"

#include "Constants.hpp"
#include "LevelObjects.hpp"
#include "Projectile.hpp"
#include "audio/Audio.hpp"
#include "entities/Entity.hpp"
#include "GameState.hpp"

// auto generated assets
#include "assets.hpp"


using namespace blit;


extern GameSaveData gameSaveData;
extern PlayerSaveData allPlayerSaveData[2];
extern LevelSaveData allLevelSaveData[2][LEVEL_COUNT];

extern float dt;
extern std::vector<Tile> foreground;
extern std::vector<Tile> platforms;
extern std::vector<LevelTrigger> levelTriggers;
extern uint8_t playerSelected;
extern LevelData levelData;
extern std::vector<Projectile> projectiles;
extern AudioHandler::AudioHandler audioHandler;
extern uint16_t levelDeathBoundary;
extern uint8_t currentLevelNumber;
extern uint8_t currentWorldNumber;
extern std::vector<Enemy> enemies;
extern std::vector<ImageParticle> imageParticles;
extern std::vector<Projectile> projectiles;
extern std::vector<Tile> foreground;
extern std::vector<Tile> generic_entities;
extern std::vector<Tile> background;
extern std::vector<Tile> platforms;
extern std::vector<Tile> spikes;
extern std::vector<ParallaxTile> parallax;
extern std::vector<Coin> coins;
extern Finish finish;
extern AnimatedTransition transition[];
extern Checkpoint checkpoint;
extern std::vector<LevelTrigger> levelTriggers;
extern std::vector<Enemy> enemies;
extern std::vector<Boss> bosses; // used for levels where there is a boss.
extern Player player;
extern uint16_t cameraStartX, cameraStartY;
extern uint16_t playerStartX, playerStartY;
extern Camera camera;
extern float snowGenTimer;
extern float confettiGenTimer;
extern const uint8_t* asset_levels[];


uint16_t get_version(GameVersion gameVersionData) {
    // return VERSION_MAJOR * 256 + VERSION_MINOR * 16 + VERSION_BUILD;
    return gameVersionData.major * 256 + gameVersionData.minor * 16 + gameVersionData.build;
}

GameVersion get_version_struct(uint16_t version) {
    GameVersion gameVersionData{};

    gameVersionData.major = version / 256;
    version %= 256;

    gameVersionData.minor = version / 16;
    version %= 16;

    gameVersionData.build = version;

    return gameVersionData;
}


void save_game_data() {
    // Write save data
    write_save(gameState.gameSaveData);
}

void save_level_data(uint8_t playerID, uint8_t levelNumber) {
    // Write level data
    write_save(gameState.allLevelSaveData[playerID][levelNumber], (playerID * (BYTE_SIZE + 1)) + 1 + levelNumber + 1);
}

void save_player_data(uint8_t playerID) {
    // Write level data
    write_save(gameState.allPlayerSaveData[playerID], (playerID * (BYTE_SIZE + 1)) + 1);
}

LevelSaveData load_level_data(uint8_t playerID, uint8_t levelNumber) {
    LevelSaveData levelSaveData;
    if (read_save(levelSaveData, (playerID * (BYTE_SIZE + 1)) + 1 + levelNumber + 1)) {
        // Success
    }
    else {
        // Set some defaults
        levelSaveData.score = 0;
        levelSaveData.enemiesKilled = 0;
        levelSaveData.time = 0.0f; // If time == 0.0f, game realises that it's a N/A time
    }
    return levelSaveData;
}

PlayerSaveData load_player_data(uint8_t playerID) {
    PlayerSaveData playerSaveData;
    if (read_save(playerSaveData, (playerID * (BYTE_SIZE + 1)) + 1)) {
        // Success
    }
    else {
        // Set some defaults
        playerSaveData.levelReached = 0;
    }
    return playerSaveData;
}

// struct to handle level data header...
// this will probably need to be revisited/reworked if 32blit-tools has *any* kind of update to it...
#pragma pack(push,1)
struct TMX16 {
    char head[4];
    uint16_t header_length;
    uint16_t flags;
    uint16_t empty_tile;
    uint16_t width;
    uint16_t height;
    uint16_t layers;
    uint16_t data[];
};
#pragma pack(pop)

const float LEVEL_DEATH_BOUNDARY_SCALE = 1.5f;

const uint16_t SNOW_LEVEL_INIT_COUNT = 300;
const uint16_t SNOW_LEVEL_SELECT_INIT_COUNT = 200;

const uint16_t CONFETTI_INIT_COUNT = 400;

const std::vector<uint16_t> coinFrames = { TILE_ID_COIN, TILE_ID_COIN + 1, TILE_ID_COIN + 2, TILE_ID_COIN + 3, TILE_ID_COIN + 2, TILE_ID_COIN + 1 };
const std::vector<uint16_t> finishFrames = { TILE_ID_FINISH, TILE_ID_FINISH + 1, TILE_ID_FINISH + 2, TILE_ID_FINISH + 3, TILE_ID_FINISH + 4, TILE_ID_FINISH + 5 };

const uint8_t* asset_levels[] = {
        asset_level0,
        asset_level1,
        asset_level2,
        asset_level3,
        asset_level4,
        asset_level5,
        asset_level6,
        asset_level7,
        asset_level8,
        asset_level9,
#ifdef PICO_BUILD
        asset_level10,
#endif // PICO_BUILD
        asset_level_title,
        asset_level_char_select,
        asset_level_level_select
};

void load_level(uint8_t levelNumber) {
    gameState.snowGenTimer = 0.0f;
    gameState.confettiGenTimer = 0.0f;

    // Variables for finding start and finish positions
    uint16_t finishX, finishY;
    uint16_t checkpointX, checkpointY;

    gameState.playerStartX = gameState.playerStartY = 0;
    gameState.cameraStartX = gameState.cameraStartY = 0;
    finishX = finishY = 0;
    checkpointX = checkpointY = 0;


    // Get a pointer to the map header
    TMX16* tmx = (TMX16*)asset_levels[levelNumber];

    uint16_t levelWidth = tmx->width;
    uint16_t levelHeight = tmx->height;
    uint32_t levelSize = levelWidth * levelHeight;

    gameState.levelData.levelWidth = levelWidth;
    gameState.levelData.levelHeight = levelHeight;

    gameState.levelDeathBoundary = gameState.levelData.levelHeight * SPRITE_SIZE * LEVEL_DEATH_BOUNDARY_SCALE;

    gameState.foreground.clear();
    gameState.generic_entities.clear();
    gameState.background.clear();
    gameState.platforms.clear();
    gameState.parallax.clear();
    gameState.coins.clear();
    gameState.enemies.clear();
    gameState.bosses.clear();
    gameState.levelTriggers.clear();
    gameState.projectiles.clear();
    gameState.imageParticles.clear();
    gameState.spikes.clear();

    // Foreground Layer
    for (uint32_t i = 0; i < levelSize; i++) {
        if (tmx->data[i] == TILE_ID_EMPTY) {
            // Is a blank tile, don't do anything
        }
        else if (tmx->data[i] == TILE_ID_COIN) {
            gameState.coins.push_back(Coin((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, coinFrames));
        }
        else {

            uint16_t x = i % levelWidth;
            uint16_t y = i / levelWidth;

            gameState.foreground.push_back(Tile(x * SPRITE_SIZE, y * SPRITE_SIZE, tmx->data[i]));
        }
    }

    if (gameState.gameSaveData.hackyFastMode < 2) {
        // Background Layer
        for (uint32_t i = 0; i < levelSize; i++) {
            uint32_t index = i + levelSize * 3;

            if (tmx->data[index] == TILE_ID_EMPTY) {
                // Is a blank tile, don't do anything
            }
            else {
                // Background tiles are non-solid. If semi-solidity (can jump up but not fall through) is required, use platforms (will be a separate layer).
                gameState.background.push_back(Tile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index]));
            }
        }
    }

    // Entity Spawns Layer
    for (uint32_t i = 0; i < levelSize; i++) {
        uint32_t index = i + levelSize;

        if (tmx->data[index] == TILE_ID_EMPTY) {
            // Is a blank tile, don't do anything
        }
        else if (tmx->data[index] == TILE_ID_PLAYER_1) {
            gameState.playerStartX = (i % levelWidth) * SPRITE_SIZE;
            gameState.playerStartY = (i / levelWidth) * SPRITE_SIZE;
        }
        else if (tmx->data[index] == TILE_ID_CAMERA) {
            gameState.cameraStartX = (i % levelWidth) * SPRITE_SIZE;
            gameState.cameraStartY = (i / levelWidth) * SPRITE_SIZE;
        }
        else if (tmx->data[index] == TILE_ID_FINISH) {
            finishX = (i % levelWidth) * SPRITE_SIZE;
            finishY = (i / levelWidth) * SPRITE_SIZE;
        }
        else if (tmx->data[index] == TILE_ID_CHECKPOINT) {
            if (gameState.gameSaveData.checkpoints) {
                checkpointX = (i % levelWidth) * SPRITE_SIZE;
                checkpointY = (i / levelWidth) * SPRITE_SIZE;
            }
        }
        else if (tmx->data[index] == TILE_ID_LEVEL_TRIGGER) {
            gameState.levelTriggers.push_back(LevelTrigger((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, 0));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_1) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[0], 0));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_2) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[1], 1));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_3) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[2], 2));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_4) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[3], 3));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_5) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[4], 4));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_6) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[5], 5));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_7) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[6], 6));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_8) {
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[7], 7));
        }
        else if (tmx->data[index] == TILE_ID_ENEMY_9) {
            // A ninth enemy!?
            gameState.enemies.push_back(Enemy((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, enemyHealths[8], 8));
        }
        else if (tmx->data[index] == TILE_ID_BOSS_1) {
            gameState.bosses.push_back(Boss((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, bossHealths[0], 0));
        }
        else if (tmx->data[index] == TILE_ID_BOSS_2) {
            gameState.bosses.push_back(Boss((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, bossHealths[1], 1));
        }
        else if (tmx->data[index] == TILE_ID_BIG_BOSS) {
            gameState.bosses.push_back(Boss((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, bossHealths[2], 2));
        }
        else if (tmx->data[index] == TILE_ID_SPIKE_BOTTOM ||
                 tmx->data[index] == TILE_ID_SPIKE_TOP ||
                 tmx->data[index] == TILE_ID_SPIKE_LEFT ||
                 tmx->data[index] == TILE_ID_SPIKE_RIGHT) {

            gameState.spikes.push_back(Tile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index]));
        }
        else if (tmx->data[index] < BYTE_SIZE) {
            // Don't create tiles for bosses etc (no terrain/non-enemy entity tiles are >=256)
            // Background tiles are non-solid
            gameState.generic_entities.push_back(Tile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index]));
        }
    }

    // Sort levelTriggers by x and relabel
    std::sort(gameState.levelTriggers.begin(), gameState.levelTriggers.end(),
            // Function used for vector sorting
              [](const LevelTrigger& a, const LevelTrigger& b) -> bool { return a.x < b.x; });
    for (uint8_t i = 0; i < gameState.levelTriggers.size(); i++) {
        gameState.levelTriggers[i].levelNumber = i;
    }

    // Platform Layer
    for (uint32_t i = 0; i < levelSize; i++) {
        uint32_t index = i + levelSize * 2;

        if (tmx->data[index] == TILE_ID_EMPTY) {
            // Is a blank tile, don't do anything
        }
        else if (levelNumber == LEVEL_SELECT_NUMBER && tmx->data[index] >= TILE_ID_LEVEL_BRIDGE_MIN) {
            // For now, only load these into normal collection of platforms if player has unlocked a level after this location
            if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached == LEVEL_COUNT || (i % levelWidth) * SPRITE_SIZE < gameState.levelTriggers[gameState.allPlayerSaveData[gameState.playerSelected].levelReached].x) {
                gameState.platforms.push_back(Tile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index]));//,PLATFORM_BOUNCE
            }
        }
        else {
            // Background tiles are non-solid. If semi-solidity (can jump up but not fall through) is required, use platforms (will be a separate layer).
            gameState.platforms.push_back(Tile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index]));
        }
    }

    // maybe adjust position of tile so that don't need to bunch all up in corner while designing level

    // go backwards through parallax layers so that rendering is correct

    if (gameState.gameSaveData.hackyFastMode < 1) {
        // Parallax Background Layer
        for (uint32_t i = 0; i < levelSize; i++) {
            uint32_t index = i + levelSize * 5;

            if (tmx->data[index] == TILE_ID_EMPTY) {
                // Is a blank tile, don't do anything
            }
            else {
                gameState.parallax.push_back(ParallaxTile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index], 1));
            }
        }

        // Parallax Foreground Layer
        for (uint32_t i = 0; i < levelSize; i++) {
            uint32_t index = i + levelSize * 4;

            if (tmx->data[index] == TILE_ID_EMPTY) {
                // Is a blank tile, don't do anything
            }
            else {
                gameState.parallax.push_back(ParallaxTile((i % levelWidth) * SPRITE_SIZE, (i / levelWidth) * SPRITE_SIZE, tmx->data[index], 0));
            }
        }
    }


    // Reset player attributes
    gameState.player = Player(gameState.playerStartX, gameState.playerStartY, gameState.playerSelected);

    gameState.finish = Finish(finishX, finishY, finishFrames);

    gameState.checkpoint = Checkpoint(checkpointX, checkpointY);

    // Reset camera position
    gameState.camera.x = gameState.cameraStartX;
    gameState.camera.y = gameState.cameraStartY;

    // Set player position pointers
    for (uint8_t i = 0; i < gameState.enemies.size(); i++) {
        gameState.enemies[i].set_player_position(&gameState.player.x, &gameState.player.y);
    }
    for (uint8_t i = 0; i < gameState.bosses.size(); i++) {
        gameState.bosses[i].set_player_position(&gameState.player.x, &gameState.player.y);
    }


    // Check there aren't any levelTriggers which have levelNumber >= LEVEL_COUNT
    gameState.levelTriggers.erase(std::remove_if(gameState.levelTriggers.begin(), gameState.levelTriggers.end(), [](LevelTrigger& levelTrigger) { return levelTrigger.levelNumber >= LEVEL_COUNT; }), gameState.levelTriggers.end());

    // Prep snow particles (create some so that it isn't empty to start with)
    if (gameState.currentScene->instanceOf(&scene::levelSelect)) {
        for (uint16_t i = 0; i < SNOW_LEVEL_SELECT_INIT_COUNT; i++) {
            // Generate snow particles
            // Get random position
            float xVel = rand() % 3 - 1;
            float yVel = rand() % 5 + 8;
            uint16_t startX = (gameState.levelTriggers[(SNOW_WORLD * LEVELS_PER_WORLD) - 1].x + gameState.levelTriggers[SNOW_WORLD * LEVELS_PER_WORLD].x) / 2;
            //uint16_t endX = (levelTriggers[((SNOW_WORLD + 1) * LEVELS_PER_WORLD) - 1].x + levelTriggers[(SNOW_WORLD + 1) * LEVELS_PER_WORLD].x) / 2;
            uint16_t endX = (gameState.levelTriggers[((SNOW_WORLD + 1) * LEVELS_PER_WORLD)].x + gameState.levelTriggers[((SNOW_WORLD + 1) * LEVELS_PER_WORLD) + 1].x) / 2;
#ifdef PICO_BUILD
            endX = levelWidth;
#endif // PICO_BUILD
            float x = (rand() % (endX - startX)) + startX;
            float y = (rand() % (gameState.levelData.levelHeight * SPRITE_SIZE + SCREEN_HEIGHT)) - SCREEN_MID_HEIGHT;
#ifdef PICO_BUILD
            if ((x > gameState.levelTriggers[SNOW_WORLD * LEVELS_PER_WORLD].x) || rand() % 2 == 0) { // && x < gameState.levelTriggers[(SNOW_WORLD + 1) * LEVELS_PER_WORLD].x
#else
            if ((x > gameState.levelTriggers[SNOW_WORLD * LEVELS_PER_WORLD].x && x < gameState.levelTriggers[(SNOW_WORLD + 1) * LEVELS_PER_WORLD].x) || rand() % 2 == 0) {
#endif // PICO_BUILD
                // At edges, only make a half as many particles
                gameState.imageParticles.push_back(ImageParticle(x, y, xVel, yVel, 0, 0, snowParticleImages[rand() % snowParticleImages.size()]));
            }
        }
    }
    else if (gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9) {
        // Generate snow particles
        for (uint16_t i = 0; i < SNOW_LEVEL_INIT_COUNT; i++) {
            // Get random position
            // Change vel later (use wind?)
            float xVel = rand() % 5 - 2;
            float yVel = rand() % 5 + 8;
            float x = (rand() % (gameState.levelData.levelWidth * SPRITE_SIZE + SCREEN_WIDTH)) - SCREEN_MID_WIDTH;
            float y = (rand() % (gameState.levelData.levelHeight * SPRITE_SIZE + SCREEN_HEIGHT)) - SCREEN_MID_HEIGHT;
            gameState.imageParticles.push_back(ImageParticle(x, y, xVel, yVel, 0, 0, snowParticleImages[rand() % snowParticleImages.size()]));
        }
    }
    else if (gameState.currentLevelNumber == LEVEL_COUNT - 1) {
        // Generate snow particles
        for (uint16_t i = 0; i < CONFETTI_INIT_COUNT; i++) {
            // Get random position
            // Change vel later (use wind?)
            float xVel = rand() % 5 - 2;
            float yVel = rand() % 5 + 8;
            uint16_t startX = 0;// SPRITE_SIZE * 5;
            uint16_t endX = SPRITE_SIZE * 45;//SPRITE_SIZE * 40;
            float x = (rand() % (endX - startX)) + startX;
            float y = (rand() % (gameState.levelData.levelHeight * SPRITE_SIZE + SCREEN_HEIGHT)) - SCREEN_MID_HEIGHT;

            gameState.imageParticles.push_back(ImageParticle(x, y, xVel, yVel, 0, 0, confettiParticleImages[rand() % confettiParticleImages.size()]));
        }
    }
}
