#include "Projectile.hpp"

#include "engine/api.hpp"


using namespace blit;


Projectile::Projectile() {
    x = y = 0;
    xVel = yVel = 0;

    id = 0;

    gravity = false;

    width = SPRITE_HALF;
}

Projectile::Projectile(float xPosition, float yPosition,
                       float xVelocity, float yVelocity,
                       uint16_t tileId, bool gravity, uint8_t rectWidth) {
    x = xPosition;
    y = yPosition;
    xVel = xVelocity;
    yVel = yVelocity;
    id = tileId;
    this->gravity = gravity;
    width = rectWidth;
}

void Projectile::update(float dt, ButtonStates& buttonStates) {
    if (gravity) {
        // Update gravity
        yVel += PROJECTILE_GRAVITY * dt;
        yVel = std::min(yVel, (float)PROJECTILE_GRAVITY_MAX);
    }

    // Move entity y
    y += yVel * dt;

    // Move entity x
    x += xVel * dt;
}

void Projectile::render(Camera& camera) {
    render_sprite(id, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
    //screen.sprite(id, Point(SCREEN_MID_WIDTH + x - camera.x - SPRITE_QUARTER, SCREEN_MID_HEIGHT + y - camera.y - SPRITE_QUARTER));
    //screen.rectangle(Rect(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y, 4, 4));
}

bool Projectile::is_colliding(float playerX, float playerY) {
    return x + SPRITE_HALF + width / 2 > playerX
        && x + SPRITE_HALF - width / 2 < playerX + SPRITE_SIZE
        && y + SPRITE_HALF + width / 2 > playerY
        && y + SPRITE_HALF - width / 2 < playerY + SPRITE_SIZE;
}