#include "Camera.hpp"

#include "engine/api.hpp"

Camera::Camera() {
    x = y = 0;
    locked = false;
    reset_temp();
}

void Camera::reset_temp() {
    tempX = tempY = 0;
}

void Camera::reset_timer() {
    timer = -1.0f;
}

bool Camera::timer_started() {
    return timer >= 0;
}

void Camera::start_timer() {
    timer = 0.0f;
}

void Camera::update_timer(float dt) {
    timer += dt;
}

float Camera::get_timer() {
    return timer;
}

void Camera::ease_out_to(float dt, float targetX, float targetY) {
    if (!locked) {
        x += (targetX - x) * CAMERA_SCALE_X * dt;
        y += (targetY - y) * CAMERA_SCALE_Y * dt;
    }
}

void Camera::linear_to(float dt, float startX, float startY, float targetX, float targetY, float time) {
    if (!locked) {
        if (std::abs(targetX - x) < std::abs(((targetX - startX) / time) * dt)) {
            x = targetX;
        }
        else {
            x += ((targetX - startX) / time) * dt;
        }

        if (std::abs(targetY - y) < std::abs(((targetY - startY) / time) * dt)) {
            y = targetY;
        }
        else {
            y += ((targetY - startY) / time) * dt;
        }
    }
}


ScreenShake::ScreenShake() {
    set_shake(0);
    set_shakiness(0);
}

ScreenShake::ScreenShake(float shakiness) {
    set_shake(0);
    set_shakiness(shakiness);
}

void ScreenShake::set_shake(float shake) {
    this->shake = shake;
}

void ScreenShake::set_shakiness(float shakiness) {
    this->shakiness = 100 / shakiness;
}

float ScreenShake::time_to_shake(float dt) {
    if (shake > 0) {
        shake -= dt;
        if (shake < 0) {
            shake = 0;
        }
        return (rand() % 201 - 100) / shakiness;
    }
    else {
        return 0.0f;
    }
}