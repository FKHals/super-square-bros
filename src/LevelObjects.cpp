#include "LevelObjects.hpp"

#include "engine/engine.hpp"

#include "Constants.hpp"


using namespace blit;


const float FRAME_LENGTH = 0.15f;

const float parallaxFactorLayersX[2] = {
        0.4f,
        0.2f
};

const float parallaxFactorLayersY[2] = {
        0.4f,
        0.2f
};


LevelObject::LevelObject() {
    x = y = 0;
}

LevelObject::LevelObject(uint16_t xPosition, uint16_t yPosition) {
    x = xPosition;
    y = yPosition;
}


Tile::Tile() : LevelObject() {
    id = TILE_ID_EMPTY;
}

Tile::Tile(uint16_t xPosition, uint16_t yPosition, uint16_t tileID) : LevelObject(xPosition, yPosition) {
    id = tileID;
}

void Tile::update(float dt, ButtonStates& buttonStates) {

}

void Tile::render(Camera& camera) {
    //screen.sprite(id, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
    render_sprite(id, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
}

uint16_t Tile::get_id() {
    return id;
}


ParallaxTile::ParallaxTile() : Tile() {
    id = TILE_ID_EMPTY;
    layer = 0;
}

ParallaxTile::ParallaxTile(uint16_t xPosition, uint16_t yPosition,
                           uint16_t tileID, uint8_t parallaxLayer) : Tile(xPosition, yPosition, tileID) {
    id = tileID;
    layer = parallaxLayer;
}

void ParallaxTile::update(float dt, ButtonStates& buttonStates) {

}

void ParallaxTile::render(Camera& camera)
{
    //screen.sprite(id, Point(SCREEN_MID_WIDTH + x - (camera.x * parallaxFactorLayersX[layer]), SCREEN_MID_HEIGHT + y - (camera.y * parallaxFactorLayersY[layer])));
    // Not shifting sprite to center seems to give better coverage of parallax
    //screen.sprite(id, Point(x - (camera.x * parallaxFactorLayersX[layer]), y - (camera.y * parallaxFactorLayersY[layer])));
    render_sprite(id, Point(x - (camera.x * parallaxFactorLayersX[layer]), y - (camera.y * parallaxFactorLayersY[layer])));
}


Pickup::Pickup() : LevelObject() {
    collected = false;
}

Pickup::Pickup(uint16_t xPosition, uint16_t yPosition) : LevelObject(xPosition, yPosition) {
    collected = false;
}


AnimatedPickup::AnimatedPickup() : Pickup() {
    animationTimer = 0;
    currentFrame = 0;
}

AnimatedPickup::AnimatedPickup(uint16_t xPosition, uint16_t yPosition,
                               std::vector<uint16_t> animationFrames) : Pickup(xPosition, yPosition) {
    animationTimer = 0;
    currentFrame = 0;

    frames = animationFrames;
}

void AnimatedPickup::update(float dt, ButtonStates& buttonStates) {
    animationTimer += dt;

    if (animationTimer >= FRAME_LENGTH) {
        animationTimer -= FRAME_LENGTH;
        currentFrame++;
        currentFrame %= frames.size();
    }
}

void AnimatedPickup::render(Camera& camera) {
    if (!collected) {
        //screen.sprite(frames[currentFrame], Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
        render_sprite(frames[currentFrame],
                      Point(SCREEN_MID_WIDTH + x - camera.x,
                            SCREEN_MID_HEIGHT + y - camera.y));
    }
}


Coin::Coin() : AnimatedPickup() {

}

Coin::Coin(uint16_t xPosition, uint16_t yPosition,
           std::vector<uint16_t> animationFrames) : AnimatedPickup(xPosition, yPosition,
                                                                   animationFrames) {

}

void Coin::update(float dt, ButtonStates& buttonStates) {
    AnimatedPickup::update(dt, buttonStates);
}

void Coin::render(Camera& camera) {
    AnimatedPickup::render(camera);
}


Finish::Finish() : AnimatedPickup() {
    particleTimer = 0.0f;
}

Finish::Finish(uint16_t xPosition, uint16_t yPosition,
               std::vector<uint16_t> animationFrames) : AnimatedPickup(xPosition, yPosition,
                                                                       animationFrames) {
    particleTimer = 0.0f;
}

void Finish::update(float dt, ButtonStates& buttonStates) {
    AnimatedPickup::update(dt, buttonStates);

    /*particleTimer += dt;
    if (particleTimer >= FINISH_PARTICLE_SPAWN_DELAY) {
        particleTimer -= FINISH_PARTICLE_SPAWN_DELAY;
        particles.push_back(generate_brownian_particle(x + SPRITE_HALF, y + SPRITE_SIZE, 0, -0.5f, 50.0f, finishParticleColours, 1));
    }*/

    for (Particle& particle : particles) {
        particle.update(dt);
    }

    // Remove any particles which are too old
    particles.erase(
            std::remove_if(particles.begin(),particles.end(),
                           [](Particle& particle) { return (particle.age >= PLAYER_SLOW_PARTICLE_AGE); }),
            particles.end()
    );
}

void Finish::render(Camera& camera) {
    AnimatedPickup::render(camera);

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}


Checkpoint::Checkpoint() {
    x = y = 0;
    colour = 0;
    generateParticles = false;
    animationTimer = 0.0f;
    currentFrame = 0;
}

Checkpoint::Checkpoint(uint16_t xPosition, uint16_t yPosition) {
x = xPosition;
y = yPosition;

colour = 0;
generateParticles = false;
animationTimer = 0.0f;
currentFrame = 0;
}

const uint8_t CHECKPOINT_PARTICLE_COUNT = 250;
const float CHECKPOINT_PARTICLE_SPEED = 70.0f;

void Checkpoint::update(float dt) {
    animationTimer += dt;

    if (animationTimer >= CHECKPOINT_FRAME_LENGTH) {
        animationTimer -= CHECKPOINT_FRAME_LENGTH;
        currentFrame++;
        currentFrame %= CHECKPOINT_FRAMES;
    }

    if (generateParticles) {
        if (particles.size() == 0) {
            generateParticles = false;
        }
        else {
            for (Particle& particle : particles) {
                particle.update(dt);
            }

            // Remove any particles which are too old
            particles.erase(
                    std::remove_if(
                            particles.begin(), particles.end(),
                            [](Particle& particle) { return (particle.age >= ENTITY_DEATH_PARTICLE_AGE); }),
                    particles.end()
            );
        }
    }
}

void Checkpoint::render(Camera& camera) {
    if (x && y) {
        // Only render if not in default position

        render_sprite(TILE_ID_CHECKPOINT, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
        render_sprite(TILE_ID_CHECKPOINT - 16 + (colour * CHECKPOINT_FRAMES) + currentFrame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y - SPRITE_SIZE));

        // Particles
        for (Particle& particle : particles) {
            particle.render(camera);
        }
    }
}

bool Checkpoint::activate(uint8_t c) {
    if (colour) {
        return false;
    }
    else {
        // Generate particles, set colour
        colour = c;

        // TODO: change constants? add age?
        particles = generate_particles(x + SPRITE_HALF, y - SPRITE_QUARTER, ENTITY_DEATH_PARTICLE_GRAVITY_X, ENTITY_DEATH_PARTICLE_GRAVITY_Y, checkpointParticleColours[colour], CHECKPOINT_PARTICLE_SPEED, CHECKPOINT_PARTICLE_COUNT);
        generateParticles = true;
        return true;
    }
}

LevelTrigger::LevelTrigger(){
    x = y = 0;
    levelNumber = 0;

    visible = true;
    generateParticles = false;

    textY = textVelY = 0;
}


LevelTrigger::LevelTrigger(uint16_t xPosition, uint16_t yPosition, uint8_t levelTriggerNumber) {
    x = xPosition;
    y = yPosition;

    levelNumber = levelTriggerNumber;

    visible = true;
    generateParticles = false;

    textY = textVelY = 0;
}

const float TEXT_JUMP_VELOCITY = 80.0f;
const float TEXT_GRAVITY = 280.0f;

void LevelTrigger::update(float dt, ButtonStates& buttonStates) {
    textVelY += TEXT_GRAVITY * dt;
    if (textY > SPRITE_SIZE) {
        textY = SPRITE_SIZE;
        textVelY = -TEXT_JUMP_VELOCITY;
    }

    textY += textVelY * dt;

    if (!visible) {

        if (generateParticles) {
            if (particles.size() == 0) {
                generateParticles = false;
            }
            else {
                for (Particle& particle : particles) {
                    particle.update(dt);
                }

                // Remove any particles which are too old
                particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= ENTITY_DEATH_PARTICLE_AGE); }), particles.end());
            }
        }
        else {
            // Generate particles
            // TODO: change constants?
            particles = generate_particles(x + SPRITE_HALF, y + SPRITE_HALF, ENTITY_DEATH_PARTICLE_GRAVITY_X, ENTITY_DEATH_PARTICLE_GRAVITY_Y, levelTriggerParticleColours, ENTITY_DEATH_PARTICLE_SPEED, ENTITY_DEATH_PARTICLE_COUNT);
            generateParticles = true;
        }
    }
}

void LevelTrigger::render(Camera& camera) {
    if (visible) {
        screen.pen = Pen(levelTriggerParticleColours[1].r, levelTriggerParticleColours[1].g, levelTriggerParticleColours[1].b, levelTriggerParticleColours[1].a);
        screen.text(std::to_string(levelNumber + 1), minimal_font, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_HALF, SCREEN_MID_HEIGHT + y - camera.y - SPRITE_HALF * 3 + textY), true, TextAlign::center_center);
        //screen.sprite(TILE_ID_LEVEL_TRIGGER, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
        render_sprite(TILE_ID_LEVEL_TRIGGER, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
    }

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}

void LevelTrigger::set_active() {
    visible = false;
}