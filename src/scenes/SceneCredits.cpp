#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

namespace scene {

    void Credits::onSwitch() const {
        gameState.creditsItem = 0;

        // Load menu level
        load_level(LEVEL_COUNT);

        open_transition();
    }

    void Credits::render() const {
        render_background();

        render_level();

        render_entities();

        screen.pen = Pen(gameBackground.r, gameBackground.g, gameBackground.b,
                         hudBackground.a); // use hudBackground.a to make background semi transparent
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)); // repeat to make darker

        background_rect(0);

        uint8_t offset = 0;
#ifdef PICO_BUILD
        offset = SPRITE_HALF;
#endif // PICO_BUILD

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Credits", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        screen.text("Coding:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_HALF * 7 - offset), true,
                    center_left);
        screen.text("Artwork:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_HALF * 3 - offset), true,
                    center_left);
        screen.text("SFX:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_HALF - offset), true,
                    center_left);

        screen.text("Special Thanks:", minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_HALF * 5 - offset),
                    true, center_left);


        screen.pen = Pen(niceBlue.r, niceBlue.g, niceBlue.b);
        screen.text("Scorpion Games", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_HALF * 7 + offset), true,
                    center_right);
        screen.text("Scorpion Games", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_HALF * 3 + offset), true,
                    center_right);
        screen.text("JFXR", minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_HALF + offset),
                    true, center_right);

        screen.text("Gadgetoid", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_HALF * 5 + offset), true,
                    center_right);
        screen.text("Daft Freak", minimal_font,
                    Point(SCREEN_WIDTH - TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_HALF * 7 + offset), true,
                    center_right);

        // Press <key> to go back
        background_rect(1);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text(messageStrings[5][gameState.gameSaveData.inputType], minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9),
                    true, center_center);
    }

    void Credits::update() const {
        update_coins(gameState.dt);
        update_checkpoint(gameState.dt);

        if (gameState.transition[0].is_ready_to_open()) {
            if (gameState.menuBack) {
                gameState.menuBack = false;
                switchSceneTo(&scene::menu);
            }
        } else if (gameState.transition[0].is_open()) {
            if (gameState.buttonStates.A == 2) {
                /*audioHandler.play(0);

                if (settingsItem == 0) {
                    gameSaveData.checkpoints = !gameSaveData.checkpoints;
                }
                else if (settingsItem == 1) {
                    gameSaveData.musicVolume = !gameSaveData.musicVolume;
                    audioHandler.set_volume(7, gameSaveData.musicVolume ? DEFAULT_VOLUME : 0);
                }
                else if (settingsItem == 2) {
                    gameSaveData.sfxVolume = !gameSaveData.sfxVolume;
                    for (uint8_t i = 0; i < 7; i++) {
                        audioHandler.set_volume(i, gameSaveData.sfxVolume ? DEFAULT_VOLUME : 0);
                    }
                }*/
            } else if (gameState.buttonStates.Y == 2) {
                // Exit settings
                gameState.audioHandler.play(0);

                gameState.menuBack = true;
                close_transition();
            }
            /*else if (buttonStates.UP == 2 && settingsItem > 0) {
                settingsItem--;
                audioHandler.play(0);
            }
            else if (buttonStates.DOWN == 2 && settingsItem < SETTINGS_COUNT) {
                settingsItem++;
                audioHandler.play(0);
            }*/
        }
    }
}