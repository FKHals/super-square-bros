#include "Scene.h"

#include "engine/engine.hpp"
#include "../Effects.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"

using namespace blit;


extern uint8_t menuItem;
extern AnimatedTransition transition[];

void render_background() {
    //screen.blit(background_image, Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT), Point(0, 0), false);
    screen.pen = Pen(gameBackground.r, gameBackground.g, gameBackground.b);

    screen.clear();
}

void background_rect(uint8_t position) {
    if (position) {
        screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
        screen.rectangle(Rect(0, SCREEN_HEIGHT - (SPRITE_SIZE + 12), SCREEN_WIDTH, SPRITE_SIZE + 12));
    }
    else {
        screen.pen = Pen(hudBackground.r, hudBackground.g, hudBackground.b, hudBackground.a);
        screen.rectangle(Rect(0, 0, SCREEN_WIDTH, SPRITE_SIZE + 12));
    }
}

void open_transition() {
    for (uint16_t i = 0; i < SCREEN_TILE_SIZE; i++) {
        gameState.transition[i].open();
    }
}

void close_transition() {
    for (uint16_t i = 0; i < SCREEN_TILE_SIZE; i++) {
        gameState.transition[i].close();
    }
}

void display_stats(bool showBadges) {
    screen.text(COINS_COLLECTED, minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT - SPRITE_SIZE * 2), true, TextAlign::center_left);
    screen.text(ENEMIES_KILLED, minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT), true, TextAlign::center_left);
    screen.text(TIME_TAKEN, minimal_font, Point(TEXT_BORDER, SCREEN_MID_HEIGHT + SPRITE_SIZE * 2), true, TextAlign::center_left);


    screen.text(std::to_string(gameState.player.score), minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_SIZE, SCREEN_MID_HEIGHT - SPRITE_SIZE * 2), true, TextAlign::center_right);
    screen.text(std::to_string(gameState.player.enemiesKilled), minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_SIZE, SCREEN_MID_HEIGHT), true, TextAlign::center_right);
    //screen.text(std::to_string((int)player.levelTimer), minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_SIZE, SCREEN_MID_HEIGHT + SPRITE_SIZE * 2), true, TextAlign::center_right);

    // Trim player.levelTimer to 2dp
    std::string levelTimerString = std::to_string(gameState.player.levelTimer);
    levelTimerString = levelTimerString.substr(0, levelTimerString.find('.') + 3);
    screen.text(levelTimerString, minimal_font, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_SIZE, SCREEN_MID_HEIGHT + SPRITE_SIZE * 2), true, TextAlign::center_right);


    uint8_t i, j, k;
    i = gameState.player.score >= levelTargets[gameState.currentLevelNumber][0][0] ? 0 : gameState.player.score >= levelTargets[gameState.currentLevelNumber][0][1] ? 1 : 2;
    j = gameState.player.enemiesKilled >= levelTargets[gameState.currentLevelNumber][1][0] ? 0 : gameState.player.enemiesKilled >= levelTargets[gameState.currentLevelNumber][1][1] ? 1 : 2;
    k = gameState.player.levelTimer <= levelTargetTimes[gameState.currentLevelNumber][0] ? 0 : gameState.player.levelTimer <= levelTargetTimes[gameState.currentLevelNumber][1] ? 1 : 2;

    /*screen.text("Rank:", minimal_font, Point(SPRITE_SIZE, SPRITE_HALF * 7), true, TextAlign::center_left);
    render_sprite(TILE_ID_GOLD_BADGE + i, Point(SCREEN_MID_WIDTH - SPRITE_HALF, SPRITE_SIZE * 3));*/


    render_sprite(TILE_ID_GOLD_BADGE + i, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_HALF, SCREEN_MID_HEIGHT - SPRITE_HALF * 5));
    render_sprite(TILE_ID_GOLD_BADGE + j + 12, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_HALF, SCREEN_MID_HEIGHT - SPRITE_HALF));
    render_sprite(TILE_ID_GOLD_BADGE + k + 16, Point(SCREEN_WIDTH - TEXT_BORDER - SPRITE_HALF, SCREEN_MID_HEIGHT + SPRITE_HALF * 3));


    /*render_sprite(TILE_ID_HUD_COINS, Point(SCREEN_WIDTH - SPRITE_HALF * 3, SCREEN_MID_HEIGHT - SPRITE_HALF * 5));
    render_sprite(TILE_ID_HUD_ENEMIES_KILLED, Point(SCREEN_WIDTH - SPRITE_HALF * 3, SCREEN_MID_HEIGHT - SPRITE_HALF));
    render_sprite(TILE_ID_HUD_TIME_TAKEN, Point(SCREEN_WIDTH - SPRITE_HALF * 3, SCREEN_MID_HEIGHT + SPRITE_HALF * 3));*/
}



void render_tiles(std::vector<Tile>& tiles) {
    for (Tile& tile : tiles) {
        tile.render(gameState.camera);
    }
}

void render_finish() {
    gameState.finish.render(gameState.camera);
}

void render_level() {
    // render parallax
    {
        screen.alpha = 192;
        for (ParallaxTile &tile : gameState.parallax) {
            tile.render(gameState.camera);
        }
        screen.alpha = 255;
    }

    render_tiles(gameState.background);

    if (gameState.dropPlayer) {
        screen.alpha = 128;
    }
    render_tiles(gameState.platforms);
    screen.alpha = 255;

    render_tiles(gameState.generic_entities);
    render_tiles(gameState.spikes);
    render_tiles(gameState.foreground);

    // render coins
    for (Coin &coin : gameState.coins) {
        coin.render(gameState.camera);
    }
}

void render_entities() {
    // render bosses
    for (Boss &boss : gameState.bosses) {
        boss.render(gameState.camera);
    }
    // render enemies
    for (Enemy &enemy : gameState.enemies) {
        enemy.render(gameState.camera);
    }

    gameState.checkpoint.render(gameState.camera);

    gameState.player.render(gameState.camera);

    // render projectiles
    for (Projectile &projectile : gameState.projectiles) {
        projectile.render(gameState.camera);
    }
}

void render_particles() {
    // render image particles
    for (ImageParticle &imageParticle : gameState.imageParticles) {
        imageParticle.render(gameState.camera);
    }
}

void reset_level_vars() {
    gameState.cameraRespawn = false;
    gameState.bossBattle = false;
    gameState.slowPlayer = false;
    gameState.dropPlayer = false;
    gameState.repelPlayer = false;

    gameState.gamePaused = false;
    gameState.pauseMenuItem = 0;
}


void update_enemies(float dt, ButtonStates& buttonStates) {
    for (int i = 0; i < gameState.enemies.size(); i++) {
        gameState.enemies[i].update(dt, buttonStates);
    }
}

void update_checkpoint(float dt) {
    gameState.checkpoint.update(dt);
}

void update_coins(float dt) {
    for (int i = 0; i < gameState.coins.size(); i++) {
        gameState.coins[i].update(dt, gameState.buttonStates);
    }
}


void update_projectiles(float dt) {
    for (uint8_t i = 0; i < gameState.projectiles.size(); i++) {
        gameState.projectiles[i].update(dt, gameState.buttonStates);
    }

    //  Allow enemies to get hit?
    /*for (uint8_t i = 0; i < enemies.size(); i++) {

    }*/

    if (!gameState.player.is_immune()) {
        uint8_t projectileCount = gameState.projectiles.size();
        gameState.projectiles.erase(std::remove_if(gameState.projectiles.begin(), gameState.projectiles.end(), [](Projectile& projectile) { return projectile.is_colliding(
                gameState.player.x, gameState.player.y); }), gameState.projectiles.end());
        if (projectileCount - gameState.projectiles.size() > 0) {
            gameState.player.health -= 1;
            gameState.player.set_immune();
        }
    }


    gameState.projectiles.erase(std::remove_if(gameState.projectiles.begin(), gameState.projectiles.end(), [](Projectile& projectile) { return (std::abs(projectile.x - gameState.player.x) > SCREEN_TILE_SIZE || std::abs(projectile.y - gameState.player.y) > SCREEN_HEIGHT); }), gameState.projectiles.end());

}

void update_particles(float dt) {
    for (ImageParticle& imageParticle : gameState.imageParticles) {
        imageParticle.update(dt);
    }

    if (gameState.currentScene->instanceOf(&scene::levelSelect)) {
        gameState.snowGenTimer += dt;
        while (gameState.snowGenTimer >= SNOW_LEVEL_SELECT_GENERATE_DELAY) {
            gameState.snowGenTimer -= SNOW_LEVEL_SELECT_GENERATE_DELAY;
            // Generate snow particles
            // Get random position
            float xVel = rand() % 3 - 1;
            float yVel = rand() % 5 + 8;
            uint16_t startX = (gameState.levelTriggers[(SNOW_WORLD * LEVELS_PER_WORLD) - 1].x + gameState.levelTriggers[SNOW_WORLD * LEVELS_PER_WORLD].x) / 2;
            //uint16_t endX = (levelTriggers[((SNOW_WORLD + 1) * LEVELS_PER_WORLD) - 1].x + levelTriggers[(SNOW_WORLD + 1) * LEVELS_PER_WORLD].x) / 2;
            uint16_t endX = (gameState.levelTriggers[((SNOW_WORLD + 1) * LEVELS_PER_WORLD)].x + gameState.levelTriggers[((SNOW_WORLD + 1) * LEVELS_PER_WORLD) + 1].x) / 2;
#ifdef PICO_BUILD
            endX = gameState.levelData.levelWidth;
#endif // PICO_BUILD
            float x = (rand() % (endX - startX)) + startX;
#ifdef PICO_BUILD
            if ((x > gameState.levelTriggers[SNOW_WORLD * LEVELS_PER_WORLD].x) || rand() % 2 == 0) { // && x < gameState.levelTriggers[(SNOW_WORLD + 1) * LEVELS_PER_WORLD].x
#else
            if ((x > gameState.levelTriggers[SNOW_WORLD * LEVELS_PER_WORLD].x && x < gameState.levelTriggers[(SNOW_WORLD + 1) * LEVELS_PER_WORLD].x) || rand() % 2 == 0) {
#endif // PICO_BUILD
                // At edges, only make a half as many particles
                gameState.imageParticles.push_back(ImageParticle(x, -SPRITE_SIZE * 8, xVel, yVel, 0, 0, snowParticleImages[rand() % snowParticleImages.size()]));
            }
        }
    }
    else if (gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9) {
        gameState.snowGenTimer += dt;
        while (gameState.snowGenTimer >= SNOW_LEVEL_GENERATE_DELAY) {
            gameState.snowGenTimer -= SNOW_LEVEL_GENERATE_DELAY;
            // Generate snow particles
            // Get random position
            float xVel = rand() % 3 - 1;
            float yVel = rand() % 5 + 8;
            float x = (rand() % (gameState.levelData.levelWidth * SPRITE_SIZE + SCREEN_WIDTH)) - SCREEN_MID_WIDTH;
            gameState.imageParticles.push_back(ImageParticle(x, -SPRITE_SIZE * 8, xVel, yVel, 0, 0, snowParticleImages[rand() % snowParticleImages.size()]));
        }
    }

    gameState.imageParticles.erase(std::remove_if(gameState.imageParticles.begin(), gameState.imageParticles.end(), [](ImageParticle& particle) { return particle.y > gameState.levelDeathBoundary * 1.3f; }), gameState.imageParticles.end());
}

void create_confetti(float dt) {
    gameState.confettiGenTimer += dt;
    while (gameState.confettiGenTimer >= CONFETTI_GENERATE_DELAY) {
        gameState.confettiGenTimer -= CONFETTI_GENERATE_DELAY;
        // Generate snow particles
        // Get random position
        float xVel = rand() % 5 - 2;
        float yVel = rand() % 5 + 8;
        uint16_t startX = 0;// SPRITE_SIZE * 5;
        uint16_t endX = SPRITE_SIZE * 45;//SPRITE_SIZE * 40;
        float x = (rand() % (endX - startX)) + startX;

        gameState.imageParticles.push_back(ImageParticle(x, 0, xVel, yVel, 0, 0, confettiParticleImages[rand() % confettiParticleImages.size()]));
    }
}

void update_thankyou(float dt) {
    gameState.thankyouValue += THANKYOU_SPEED * dt;
}