#include "Scene.h"

#include "engine/engine.hpp"
#include "../Constants.hpp"
#include "../Effects.hpp"
#include "../audio/Audio.hpp"
#include "../Data.hpp"
#include "../GameState.hpp"


using namespace blit;

extern Colour splashColour;
extern ButtonStates buttonStates;
extern GameSaveData gameSaveData;
extern AudioHandler::AudioHandler audioHandler;

extern float textFlashTimer;
extern AnimatedTransition transition[];

namespace scene {

    void Menu::onSwitch() const {
        gameState.menuItem = 0;

        // Load menu level
        load_level(LEVEL_COUNT);

        open_transition();
    }

    void Menu::render() const {
        render_background();

        render_level();

        render_entities();

        background_rect(0);
        background_rect(1);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        screen.text("Super Square Bros.", minimal_font, Point(SCREEN_MID_WIDTH, 10), true, center_center);

        if (gameState.menuItem == 0) {
            screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
        }
        screen.text("Play", minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_MID_HEIGHT - 20), true, center_center);

        if (gameState.menuItem == 1) {
            screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
        } else {
            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        }
        screen.text("Settings", minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_MID_HEIGHT), true, center_center);

        if (gameState.menuItem == 2) {
            screen.pen = Pen(inputSelectColour.r, inputSelectColour.g, inputSelectColour.b);
        } else {
            screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);
        }
        screen.text("Credits", minimal_font, Point(SCREEN_MID_WIDTH, SCREEN_MID_HEIGHT + 20), true, center_center);

        screen.pen = Pen(defaultWhite.r, defaultWhite.g, defaultWhite.b);

        if (gameState.textFlashTimer < TEXT_FLASH_TIME * 0.6f) {
            screen.text(messageStrings[4][gameState.gameSaveData.inputType], minimal_font,
                        Point(SCREEN_MID_WIDTH, SCREEN_HEIGHT - 9), true, center_center);
        }
    }

    void Menu::update() const {
        update_coins(gameState.dt);
        update_checkpoint(gameState.dt);

        if (gameState.splashColour.a > 0) {
            if (gameState.splashColour.a >= FADE_STEP) {
                gameState.splashColour.a -= FADE_STEP;
            } else {
                gameState.splashColour.a = 0;
            }
        } else {
            if (gameState.transition[0].is_ready_to_open()) {
                if (gameState.menuBack) {
                    gameState.menuBack = false;
                    // start input select
                    {
                        switchSceneTo(&scene::inputSelect);

                        open_transition();
                    }
                } else {
                    if (gameState.menuItem == 0) {
                        switchSceneTo(&scene::characterSelect);
                    } else if (gameState.menuItem == 1) {
                        // start settings
                        {
                            switchSceneTo(&scene::settings);

                            gameState.settingsItem = 0;

                            // Load menu level
                            load_level(LEVEL_COUNT);

                            open_transition();
                        }
                    } else {
                        // start credits
                        {
                            switchSceneTo(&scene::credits);

                            gameState.creditsItem = 0;

                            // Load menu level
                            load_level(LEVEL_COUNT);

                            open_transition();
                        }
                    }
                }
            } else if (gameState.transition[0].is_open()) {
                if (gameState.buttonStates.A == 2) {
                    gameState.audioHandler.play(0);

                    close_transition();
                } else if (gameState.buttonStates.Y == 2) {
                    gameState.audioHandler.play(0);

                    gameState.menuBack = true;
                    close_transition();
                } else if (gameState.buttonStates.UP == 2 && gameState.menuItem > 0) {
                    gameState.menuItem--;
                    gameState.audioHandler.play(0);
                } else if (gameState.buttonStates.DOWN == 2 && gameState.menuItem < 2) {
                    gameState.menuItem++;
                    gameState.audioHandler.play(0);
                }
            }
        }
    }
}