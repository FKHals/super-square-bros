#ifndef SUPER_SQUARE_BROS_INPUT_H
#define SUPER_SQUARE_BROS_INPUT_H

#include "engine/api.hpp"

enum InputType {
    CONTROLLER = 0,
    KEYBOARD = 1
    //NO_INPUT_TYPE
};

struct ButtonStates {
    uint8_t UP;
    uint8_t DOWN;
    uint8_t LEFT;
    uint8_t RIGHT;
    uint8_t A;
    uint8_t B;
    uint8_t X;
    uint8_t Y;
    uint8_t JOYSTICK;

    void update();
};

#endif //SUPER_SQUARE_BROS_INPUT_H
