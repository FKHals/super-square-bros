#include "Constants.hpp"

#include "engine/engine.hpp"


using namespace blit;


void render_sprite(uint16_t id, Point point) {
    //screen.sprite(id, point, Point(0, 0), 1.0f, SpriteTransform::NONE);
    screen.sprite(id, point);
}

void render_sprite(uint16_t id, Point point, SpriteTransform transform) {
    //screen.sprite(id, point, Point(0, 0), 1.0f, transform);
    screen.sprite(id, point, transform);
}
