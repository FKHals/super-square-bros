#ifndef SUPER_SQUARE_BROS_GAMESTATE_H
#define SUPER_SQUARE_BROS_GAMESTATE_H

#include "engine/api.hpp"
#include "engine/engine.hpp"
#include "engine/version.hpp"

#include "Camera.hpp"
#include "GameInput.hpp"
#include "Constants.hpp"
#include "Effects.hpp"
#include "LevelObjects.hpp"
#include "entities/Entity.hpp"
#include "scenes/Scene.h"
#include "Data.hpp"
#include "audio/Audio.hpp"


using namespace blit;


struct GameState {
    uint16_t levelDeathBoundary;
    float dt;
    uint32_t lastTime;
    AudioHandler::AudioHandler audioHandler;
    bool menuBack;  // tells menu to go backwards instead of forwards.
    bool gamePaused;  // used for determining if game is paused or not.
    bool coinSfxAlternator;  // used for alternating channel used for coin pickup sfx
    bool cameraIntro;
    bool cameraRespawn;
    bool cameraNewWorld;
    uint16_t cameraStartX;
    uint16_t cameraStartY;
    uint16_t playerStartX;
    uint16_t playerStartY;
    float textFlashTimer;
    uint8_t playerSelected;
    uint8_t pauseMenuItem;
    uint8_t menuItem;
    uint8_t settingsItem;
    uint8_t creditsItem;
    float snowGenTimer;
    float confettiGenTimer;
    bool slowPlayer;
    bool dropPlayer;
    bool repelPlayer;
    bool bossBattle;
    float thankyouValue;
    uint8_t currentLevelNumber;
    uint8_t currentWorldNumber;
    GameVersion gameVersion;
    // For metadata
    GameMetadata metadata;
    Scene const * currentScene;
    ButtonStates buttonStates;
    GameSaveData gameSaveData;
    PlayerSaveData allPlayerSaveData[2];
    LevelSaveData allLevelSaveData[2][LEVEL_COUNT];
    LevelData levelData;
    ScreenShake shaker;
    Colour splashColour;
    Camera camera;
    std::vector<ImageParticle> imageParticles;
    std::vector<Projectile> projectiles;
    std::vector<Tile> foreground;
    std::vector<Tile> generic_entities;  // used because platforms are rendered over background, but bushes, trees etc need to be rendered over platforms
    std::vector<Tile> background;
    std::vector<Tile> platforms;  // Platforms are only collidable when falling (or travelling sideways)
    std::vector<Tile> spikes;
    std::vector<ParallaxTile> parallax;
    std::vector<Coin> coins;
    Finish finish;
    AnimatedTransition transition[SCREEN_TILE_SIZE];
    Checkpoint checkpoint;
    std::vector<LevelTrigger> levelTriggers;
    std::vector<Enemy> enemies;
    std::vector<Boss> bosses;  // used for levels where there is a boss.
    Player player;

    GameState();

};

extern GameState gameState;

void switchSceneTo(Scene const * nextScene);

#endif //SUPER_SQUARE_BROS_GAMESTATE_H
