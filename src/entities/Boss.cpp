#include "Entity.hpp"

#include "../GameState.hpp"


const float BOSS_INJURED_TIME = 0.3f;
const float BOSS_IMMUNE_TIME = 1.0f;

const float BOSS_1_IDLE_SPEED = 30.0f;
const float BOSS_1_PURSUIT_SPEED = 50.0f;
const float BOSS_1_ANGRY_SPEED = 105.0f;
const float BOSS_1_SPEED_REDUCTION_SCALE = 0.7f;
const float BOSS_1_JUMP_SPEED = 160.0f;
const float BOSS_1_ANGRY_JUMP_SPEED = 220.0f;
const float BOSS_1_JUMP_COOLDOWN = 1.5f;
const float BOSS_1_MINION_SPAWN_COOLDOWN = 1.5f;
const float BOSS_1_MINION_SPEED = 25.0f;
const float BOSS_1_MINION_SPEED_REDUCTION = 4.0f;
const float BOSS_1_JUMP_TRIGGER_MAX_RANGE = SPRITE_SIZE * 8;
const float BOSS_1_IGNORE_MIN_RANGE = SPRITE_SIZE * 9;
const float BOSS_1_INJURED_MAX_RANGE = SPRITE_SIZE * 12; //todo: is this right? reduce for pico - so doesn't go off screen
const float BOSS_1_DEATH_MAX_RANGE = SPRITE_SIZE * 16;
const float BOSS_1_RETURN_TO_SPAWN_RANGE = SPRITE_SIZE * 4;
const float BOSS_1_JUMP_SHAKE_TIME = 0.3f;
const float BOSS_1_ANGRY_JUMP_SHAKE_TIME = 0.35f;

const float BOSS_2_JUMP_SPEED = 160.0f;
const float BOSS_2_ANGRY_JUMP_SPEED = 220.0f;
const float BOSS_2_JUMP_COOLDOWN = 1.5f;
const float BOSS_2_RELOAD_TIME = 2.0f;
const float BOSS_2_RAPID_RELOAD_TIME = 0.4f;
const float BOSS_2_SUPER_RAPID_RELOAD_TIME = 0.1f;
const uint8_t BOSS_2_RAPID_SHOT_COUNT = 3;
const float BOSS_2_PROJECTILE_FLIGHT_TIME = 1.1f;
const float BOSS_2_RAPID_PROJECTILE_FLIGHT_TIME = 0.8f;
const float BOSS_2_SUPER_RAPID_PROJECTILE_FLIGHT_TIME = 0.8f;
const float BOSS_2_RESET_COOLDOWN = 2.0f;
const float BOSS_2_INJURED_TIME = 0.3f;
const float BOSS_2_IMMUNE_TIME = 1.0f;
const float BOSS_2_JUMP_TRIGGER_MAX_RANGE = SPRITE_SIZE * 10;
const float BOSS_2_IGNORE_MIN_RANGE = SPRITE_SIZE * 11;
const float BOSS_2_JUMP_SHAKE_TIME = 0.3f;
const float BOSS_2_ANGRY_JUMP_SHAKE_TIME = 0.35f;

const float BIG_BOSS_IDLE_SPEED = 30.0f;
const float BIG_BOSS_PURSUIT_SPEED = 50.0f;
const float BIG_BOSS_RETREAT_SPEED = 20.0f;
//const float BIG_BOSS_ANGRY_SPEED = 100.0f;
const float BIG_BOSS_JUMP_SPEED = 180.0f;
const float BIG_BOSS_ANGRY_JUMP_SPEED = 240.0f;
const float BIG_BOSS_JUMP_COOLDOWN = 2.0f;
const float BIG_BOSS_MINION_SPAWN_COOLDOWN = 1.5f;
const float BIG_BOSS_JUMP_TRIGGER_MAX_RANGE = SPRITE_SIZE * 10;
const float BIG_BOSS_IGNORE_MIN_RANGE = SPRITE_SIZE * 12;
const float BIG_BOSS_INJURED_MAX_RANGE = SPRITE_SIZE * 10;
//const float BOSS_1_DEATH_MAX_RANGE = SPRITE_SIZE * 16;
const float BIG_BOSS_RETURN_TO_SPAWN_RANGE = SPRITE_SIZE * 4;
const float BIG_BOSS_JUMP_SHAKE_TIME = 0.45f;
const float BIG_BOSS_ANGRY_JUMP_SHAKE_TIME = 0.5f;
const float BIG_BOSS_RELOAD_TIME = 2.3f;
const float BIG_BOSS_RAPID_RELOAD_TIME = 0.4f;
const float BIG_BOSS_PROJECTILE_FLIGHT_TIME = 1.0f;

const uint8_t BOSS_DEATH_PARTICLE_COUNT = 200;
const float BOSS_DEATH_PARTICLE_GRAVITY_X = 0.0f;
const float BOSS_DEATH_PARTICLE_GRAVITY_Y = 60.0f;
const float BOSS_DEATH_PARTICLE_AGE = 1.4f;
const float BOSS_DEATH_PARTICLE_SPEED = 70.0f;


using namespace blit;

Boss::Boss() : Enemy() {
    spawnX = spawnY = 0;

    anchorFrame = TILE_ID_BOSS_1;

    currentSpeed = BOSS_1_IDLE_SPEED;

    injuredTimer = 0;
    minionsToSpawn = 0;
    dead = false;
    shakeOnLanding = 0;
}

Boss::Boss(uint16_t xPosition, uint16_t yPosition,
           uint8_t startHealth, uint8_t type) : Enemy(xPosition, yPosition,
                                                      startHealth, type) {
    spawnX = xPosition;
    spawnY = yPosition;

    anchorFrame = TILE_ID_BOSS_1 + type * 8;
    if (type == 2) {
        anchorFrame += 16;
    }

    currentSpeed = BOSS_1_IDLE_SPEED;

    injuredTimer = 0;
    minionsToSpawn = 0;
    dead = false;
    shakeOnLanding = 0;
}

void Boss::update(float dt, ButtonStates& buttonStates) {
    if (jumpCooldown) {
        jumpCooldown -= dt;
        if (jumpCooldown < 0) {
            jumpCooldown = 0;
        }
    }

    if (injuredTimer) {
        injuredTimer -= dt;
        if (injuredTimer < 0) {
            injuredTimer = 0;
        }
    }

    if (immuneTimer) {
        immuneTimer -= dt;
        if (immuneTimer < 0) {
            immuneTimer = 0;
        }
    }

    if (reloadTimer) {
        reloadTimer -= dt;
        if (reloadTimer < 0) {
            reloadTimer = 0;
        }
    }

    if (rapidfireTimer) {
        rapidfireTimer -= dt;
        if (rapidfireTimer < 0) {
            rapidfireTimer = 0;
        }
    }

    if (enemyType == EnemyType::BASIC) {
        if (lastDirection) {
            xVel = currentSpeed;
        }
        else {
            xVel = -currentSpeed;
        }

        update_collisions();

        if (state == 0) {
            // IDLE

            if (is_within_range(x, spawnX, BOSS_1_RETURN_TO_SPAWN_RANGE)) {
                // Wait
                currentSpeed = 0;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;
            }
            else {
                // Return to spawn
                currentSpeed = BOSS_1_IDLE_SPEED;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

                bool shouldJump = true;

                float tempX = lastDirection ? x + SPRITE_SIZE * 2 : x - SPRITE_SIZE * 2;
                for (Tile& tile : gameState.foreground) {
                    if (y + SPRITE_SIZE * 2 == tile.y && tile.x + SPRITE_SIZE > tempX + 1 && tile.x < tempX + SPRITE_SIZE * 2 - 1) {
                        // About to be on block
                        shouldJump = false;
                        break;
                    }
                }

                if (!shouldJump) {
                    for (Tile& tile : gameState.foreground) {
                        if ((lastDirection ? x + SPRITE_SIZE * 2 - 1 : x - SPRITE_SIZE + 1) == tile.x) {
                            // Walked into side of block
                            shouldJump = true;
                            // Break because we definitely need to jump
                            break;
                        }
                    }
                }

                if (shouldJump && !jumpCooldown) {
                    if (is_on_block()) {
                        shakeOnLanding = BOSS_1_JUMP_SHAKE_TIME;
                        jump(BOSS_1_JUMP_SPEED, BOSS_1_JUMP_COOLDOWN);
                    }
                }
            }


            // Handle states
            if (is_within_range(x, *playerX, BOSS_1_JUMP_TRIGGER_MAX_RANGE) && is_within_range(y, *playerY, BOSS_1_JUMP_TRIGGER_MAX_RANGE)) {
                state = 1;
                gameState.bossBattle = true;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;


                // JUMP
                if (is_on_block()) {
                    shakeOnLanding = BOSS_1_ANGRY_JUMP_SHAKE_TIME;
                    jump(BOSS_1_ANGRY_JUMP_SPEED, BOSS_1_JUMP_COOLDOWN);
                }
            }
        }
        else if (state == 1) {
            // PURSUE

            // Only go fast once on ground
            for (Tile& tile : gameState.foreground) {
                if (y + SPRITE_SIZE * 2 == tile.y && tile.x + SPRITE_SIZE - 1 > x && tile.x + 1 < x + SPRITE_SIZE * 2) {
                    // On block
                    currentSpeed = BOSS_1_PURSUIT_SPEED;
                }
            }

            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

            bool shouldJump = true;

            float tempX = lastDirection ? x + SPRITE_SIZE * 2 : x - SPRITE_SIZE * 2;
            for (Tile& tile : gameState.foreground) {
                if (y + SPRITE_SIZE * 2 == tile.y && tile.x + SPRITE_SIZE > tempX + 1 && tile.x < tempX + SPRITE_SIZE * 2 - 1) {
                    // About to be on block
                    shouldJump = false;
                    break;
                }
            }

            if (!shouldJump) {
                for (Tile& tile : gameState.foreground) {
                    if ((lastDirection ? x + SPRITE_SIZE * 2 - 1 : x - SPRITE_SIZE + 1) == tile.x) {
                        // Walked into side of block
                        shouldJump = true;
                        // Break because we definitely need to jump
                        break;
                    }
                }
            }


            if (shouldJump && !jumpCooldown) {
                if (is_on_block()) {
                    shakeOnLanding = BOSS_1_JUMP_SHAKE_TIME;
                    jump(BOSS_1_JUMP_SPEED, BOSS_1_JUMP_COOLDOWN);
                }
            }



            // Handle states
            if (!is_within_range(x, *playerX, BOSS_1_IGNORE_MIN_RANGE) || !is_within_range(y, *playerY, BOSS_1_IGNORE_MIN_RANGE)) {
                state = 0;
            }
            else if (is_immune()) {
                state = 2;
                // slow down player
                gameState.slowPlayer = true;
            }
        }
        else if (state == 2) {
            // IMMUNE

            currentSpeed = BOSS_1_ANGRY_SPEED - BOSS_1_SPEED_REDUCTION_SCALE * get_abs_center_range(x, *playerX);

            //printf("Immune state (2), current speed %f\n", currentSpeed);

            // Head away from player
            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 1 : 0;

            bool hitWall = false;
            for (Tile& tile : gameState.foreground) {
                if (tile.y + SPRITE_SIZE > y && tile.y < y + SPRITE_SIZE * 2 && (lastDirection ? x + SPRITE_SIZE * 2 - 1 : x - SPRITE_SIZE + 1) == tile.x) {
                    // Walked into side of block
                    hitWall = true;
                    // Break because we definitely have hit wall
                    break;
                }
            }

            //bool atEdge = true;
            //float tempX = lastDirection ? x + SPRITE_SIZE * 2 : x - SPRITE_SIZE * 2;
            //for (uint16_t i = 0; i < foreground.size(); i++) {
            //    if (y + SPRITE_SIZE * 2 == foreground[i].y && foreground[i].x + SPRITE_SIZE > tempX + 1 && foreground[i].x < tempX + SPRITE_SIZE * 2 - 1) {
            //        // About to be on block
            //        atEdge = false;
            //        break;
            //    }
            //}

            // Handle states
            if (hitWall || /*atEdge ||*/ !is_within_range(x, *playerX, health == 0 ? BOSS_1_DEATH_MAX_RANGE : BOSS_1_INJURED_MAX_RANGE) || !is_within_range(y, *playerY, health == 0 ? BOSS_1_DEATH_MAX_RANGE : BOSS_1_INJURED_MAX_RANGE)) {
                // NOTE: bug if you kill boss right on edge of platform or other times?
                state = 3;
                minionsToSpawn = 3 - health;
                jumpCooldown = BOSS_1_MINION_SPAWN_COOLDOWN; // delay minion spawning
            }
        }
        else if (state == 3) {
            // SPAWN MINIONS

            currentSpeed = 0;//BOSS_IDLE_SPEED;

            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

            // Jump whenever spawn minion
            if (!jumpCooldown && minionsToSpawn) {
                if (is_on_block()) {
                    shakeOnLanding = BOSS_1_ANGRY_JUMP_SHAKE_TIME;

                    // Spawn minion
                    gameState.enemies.push_back(Enemy(x + SPRITE_SIZE, y + SPRITE_SIZE, enemyHealths[(uint8_t)enemyType], (uint8_t)enemyType));
                    gameState.enemies[gameState.enemies.size() - 1].lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;
                    gameState.enemies[gameState.enemies.size() - 1].set_speed(BOSS_1_MINION_SPEED - BOSS_1_MINION_SPEED_REDUCTION * (2 - health));
                    minionsToSpawn--;

                    jump(BOSS_1_ANGRY_JUMP_SPEED, BOSS_1_MINION_SPAWN_COOLDOWN);
                }
            }

            // Handle states
            if (!jumpCooldown && !minionsToSpawn) {
                if (health > 0) {
                    // Not dead
                    state = 1;
                    immuneTimer = 0;

                    // Unslow player
                    gameState.slowPlayer = false;
                }
                else {
                    // Dead
                    // Generate particles
                    particles = generate_particles(x + SPRITE_SIZE, y + SPRITE_SIZE, BOSS_DEATH_PARTICLE_GRAVITY_X, BOSS_DEATH_PARTICLE_GRAVITY_Y, bossDeathParticleColours[(uint8_t)enemyType], BOSS_DEATH_PARTICLE_SPEED, BOSS_DEATH_PARTICLE_COUNT);
                    deathParticles = true;
                    state = 4;
                    dead = true;
                    // Play death sfx
                    gameState.audioHandler.play(3);
                }
            }
        }
        else if (state == 4) {
            // Dead, displaying particles

            if (deathParticles) {
                if (particles.size() == 0) {
                    // No particles left
                    deathParticles = false;

                    // Unslow player
                    gameState.slowPlayer = false;
                }
                else {
                    for (Particle& particle : particles) {
                        particle.update(dt);
                    }

                    // Remove any particles which are too old
                    particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= BOSS_DEATH_PARTICLE_AGE); }), particles.end());
                }
            }
        }
    }
    else if (enemyType == EnemyType::RANGED) {
        update_collisions();

        if (state == 0) {
            // IDLE
            currentSpeed = 0;

            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

            // Handle states
            if (is_within_range(x, *playerX, BOSS_2_JUMP_TRIGGER_MAX_RANGE) && is_within_range(y, *playerY, BOSS_2_JUMP_TRIGGER_MAX_RANGE)) {
                state = 1;
                gameState.bossBattle = true;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

                // JUMP
                if (is_on_block()) {
                    shakeOnLanding = BOSS_2_ANGRY_JUMP_SHAKE_TIME;
                    jump(BOSS_2_ANGRY_JUMP_SPEED, BOSS_2_JUMP_COOLDOWN);
                }
            }
        }
        else if (state == 1) {
            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

            if (!reloadTimer) {
                // Fire!
                float xV = (*playerX - x) / BOSS_2_PROJECTILE_FLIGHT_TIME;
                // yVel is broken
                float yV = ((*playerY - y) / BOSS_2_PROJECTILE_FLIGHT_TIME) - 0.5f * PROJECTILE_GRAVITY * BOSS_2_PROJECTILE_FLIGHT_TIME;

                //x,y should be offset to center
                gameState.projectiles.push_back(Projectile(x + SPRITE_SIZE, y + SPRITE_SIZE, xV, yV, gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9 ? TILE_ID_BOSS_PROJECTILE_SNOWBALL : TILE_ID_BOSS_PROJECTILE_ROCK, true, SPRITE_SIZE));
                reloadTimer = BOSS_2_RELOAD_TIME;

                gameState.audioHandler.play(6);
            }

            // Handle states
            if (!is_within_range(x, *playerX, BOSS_2_IGNORE_MIN_RANGE) || !is_within_range(y, *playerY, BOSS_2_IGNORE_MIN_RANGE)) {
                state = 0;
            }
            else if (is_immune()) {
                if (health == 0) {
                    state = 3;

                    // JUMP
                    if (is_on_block()) {
                        shakeOnLanding = BOSS_2_ANGRY_JUMP_SHAKE_TIME;
                        jump(BOSS_2_ANGRY_JUMP_SPEED, BOSS_2_JUMP_COOLDOWN);
                    }

                    shotsLeft = BOSS_2_RAPID_SHOT_COUNT * 3;

                    // Make player drop through floor
                    gameState.dropPlayer = true;
                }
                else {
                    state = 2;

                    // JUMP
                    if (is_on_block()) {
                        shakeOnLanding = BOSS_2_ANGRY_JUMP_SHAKE_TIME;
                        jump(BOSS_2_ANGRY_JUMP_SPEED, BOSS_2_JUMP_COOLDOWN);
                    }

                    shotsLeft = BOSS_2_RAPID_SHOT_COUNT + (3 - health);

                    // Make player drop through floor
                    gameState.dropPlayer = true;
                }
            }
        }
        else if (state == 2) {
            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

            if (is_on_block()) {
                if (shotsLeft && !reloadTimer) {
                    // Fire!
                    float xV = ((*playerX - x) / BOSS_2_RAPID_PROJECTILE_FLIGHT_TIME) * (1.15f - shotsLeft / 20);
                    // yVel is broken
                    float yV = ((*playerY - y) / BOSS_2_RAPID_PROJECTILE_FLIGHT_TIME) - 0.5f * PROJECTILE_GRAVITY * BOSS_2_RAPID_PROJECTILE_FLIGHT_TIME;

                    //x,y should be offset to center
                    gameState.projectiles.push_back(Projectile(x + SPRITE_SIZE, y + SPRITE_SIZE, xV, yV, gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9 ? TILE_ID_BOSS_PROJECTILE_SNOWBALL : TILE_ID_BOSS_PROJECTILE_ROCK, true, SPRITE_SIZE));
                    reloadTimer = BOSS_2_RAPID_RELOAD_TIME;

                    gameState.audioHandler.play(6);

                    shotsLeft--;
                }
            }

            if (!shotsLeft && !reloadTimer) {
                // Not dead
                state = 0;
                immuneTimer = 0;
                reloadTimer = BOSS_2_RESET_COOLDOWN;

                // Unslow player
                gameState.slowPlayer = false;
            }
        }
        else if (state == 3) {
            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;

            if (is_on_block()) {
                if (shotsLeft && !reloadTimer) {
                    // Fire!
                    float tX = x - (SPRITE_SIZE * BOSS_2_RAPID_SHOT_COUNT * 3) + SPRITE_SIZE * shotsLeft * 2;
                    float xV = ((tX - x) / BOSS_2_SUPER_RAPID_PROJECTILE_FLIGHT_TIME);
                    // yVel is broken
                    float yV = ((*playerY - y) / BOSS_2_SUPER_RAPID_PROJECTILE_FLIGHT_TIME) - 0.5f * PROJECTILE_GRAVITY * BOSS_2_SUPER_RAPID_PROJECTILE_FLIGHT_TIME;

                    //x,y should be offset to center
                    gameState.projectiles.push_back(Projectile(x, y, xV, yV, gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9 ? TILE_ID_BOSS_PROJECTILE_SNOWBALL : TILE_ID_BOSS_PROJECTILE_ROCK, true, SPRITE_SIZE));
                    reloadTimer = BOSS_2_SUPER_RAPID_RELOAD_TIME;

                    gameState.audioHandler.play(6);

                    shotsLeft--;
                }
            }

            if (!shotsLeft && !reloadTimer) {
                // Dead
                // Generate particles
                particles = generate_particles(x + SPRITE_SIZE, y + SPRITE_SIZE, BOSS_DEATH_PARTICLE_GRAVITY_X, BOSS_DEATH_PARTICLE_GRAVITY_Y, bossDeathParticleColours[(uint8_t)enemyType], BOSS_DEATH_PARTICLE_SPEED, BOSS_DEATH_PARTICLE_COUNT);
                deathParticles = true;
                state = 4;
                dead = true;
                // Play death sfx
                gameState.audioHandler.play(3);
            }
        }
        else if (state == 4) {
            // Dead, displaying particles

            if (deathParticles) {
                if (particles.size() == 0) {
                    // No particles left
                    deathParticles = false;

                    // Unslow player
                    gameState.slowPlayer = false;
                }
                else {
                    for (Particle& particle : particles) {
                        particle.update(dt);
                    }

                    // Remove any particles which are too old
                    particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= BOSS_DEATH_PARTICLE_AGE); }), particles.end());
                }
            }
        }
    }
    else if (enemyType == EnemyType::PURSUIT) {
        // Use persuit tag for giant boss, even though it's more like a BASIC v2.0
        if (lastDirection) {
            xVel = currentSpeed;
        }
        else {
            xVel = -currentSpeed;
        }

        update_collisions();

        if (state == 0) {
            // IDLE

            if (is_within_range(x, spawnX, BIG_BOSS_RETURN_TO_SPAWN_RANGE)) {
                // Wait
                currentSpeed = 0;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 0 : 1;
            }
            else {
                // Return to spawn
                currentSpeed = BIG_BOSS_IDLE_SPEED;

                lastDirection = spawnX < x + SPRITE_SIZE * 2 ? 0 : 1;
            }


            // Handle states
            if (is_within_range(x, *playerX, BIG_BOSS_JUMP_TRIGGER_MAX_RANGE) && is_within_range(y, *playerY, BIG_BOSS_JUMP_TRIGGER_MAX_RANGE)) {
                state = 1;
                gameState.bossBattle = true;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 0 : 1;

                shotsLeft = 0;


                // JUMP
                if (is_on_block()) {
                    shakeOnLanding = BIG_BOSS_ANGRY_JUMP_SHAKE_TIME;
                    jump(BIG_BOSS_ANGRY_JUMP_SPEED, BIG_BOSS_JUMP_COOLDOWN);
                }
            }
        }
        else if (state == 1) {
            // PURSUE

            if ((*playerY < y - SPRITE_SIZE * 4 && std::abs((*playerX + SPRITE_HALF) - (x + SPRITE_SIZE * 2)) < SPRITE_SIZE * 6) || std::abs((*playerX + SPRITE_HALF) - (x + SPRITE_SIZE * 2)) > SPRITE_SIZE * 9) {
                // Player is a bit above boss, FIRE!
                currentSpeed = 0;

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 0 : 1;

                if (!reloadTimer && !shotsLeft) {
                    shotsLeft = 3;
                }

                if (is_on_block()) {
                    if (!rapidfireTimer && shotsLeft) {
                        // Fire!
                        float xV = ((*playerX - x - SPRITE_HALF * 3) / BIG_BOSS_PROJECTILE_FLIGHT_TIME) * (1.1f - shotsLeft / 20);
                        // yVel is broken
                        float yV = ((*playerY - y - SPRITE_HALF * 3) / BIG_BOSS_PROJECTILE_FLIGHT_TIME) - 0.5f * PROJECTILE_GRAVITY * BIG_BOSS_PROJECTILE_FLIGHT_TIME;

                        // x,y are offset to center
                        gameState.projectiles.push_back(Projectile(x + SPRITE_SIZE * 2, y + SPRITE_SIZE, xV, yV, gameState.currentWorldNumber == SNOW_WORLD || gameState.currentLevelNumber == 8 || gameState.currentLevelNumber == 9 ? TILE_ID_BOSS_PROJECTILE_SNOWBALL : TILE_ID_BOSS_PROJECTILE_ROCK, true, SPRITE_SIZE));

                        rapidfireTimer = BIG_BOSS_RAPID_RELOAD_TIME;
                        shotsLeft--;

                        gameState.audioHandler.play(6);
                    }
                }

                if (!shotsLeft && !reloadTimer) {
                    reloadTimer = BIG_BOSS_RELOAD_TIME;
                }
            }
            else if (std::abs((*playerX + SPRITE_HALF) - (x + SPRITE_SIZE * 2)) > SPRITE_SIZE * 4 && !reloadTimer) {
                // Only go fast once on ground
                if (is_on_block()) {
                    currentSpeed = BIG_BOSS_PURSUIT_SPEED;
                }

                lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 0 : 1;

                bool shouldStop = true;

                float tempX = lastDirection ? x + SPRITE_SIZE * 2 : x - SPRITE_SIZE * 2;
                for (Tile& tile : gameState.foreground) {
                    if (y + SPRITE_SIZE * 4 == tile.y && tile.x + SPRITE_SIZE > tempX + 1 && tile.x < tempX + SPRITE_SIZE * 4 - 1) {
                        // About to be on block
                        shouldStop = false;
                        break;
                    }
                }
                if (shouldStop) {
                    currentSpeed = 0;
                }
            }
            else if (std::abs((*playerX + SPRITE_HALF) - (x + SPRITE_SIZE * 2)) < SPRITE_SIZE * 5 && *playerY > y) {
                // Only go fast once on ground
                if (is_on_block()) {
                    currentSpeed = BIG_BOSS_PURSUIT_SPEED;

                    if (!jumpCooldown) {
                        shakeOnLanding = BIG_BOSS_JUMP_SHAKE_TIME;
                        jump(BIG_BOSS_JUMP_SPEED, BIG_BOSS_JUMP_COOLDOWN);
                    }
                }
            }
            else {
                currentSpeed = 0;
            }

            // Handle states
            if (!is_within_range(x, *playerX, BIG_BOSS_IGNORE_MIN_RANGE) || !is_within_range(y, *playerY, BIG_BOSS_IGNORE_MIN_RANGE)) {
                state = 0;
            }
            else if (is_immune()) {
                state = 2;
                // slow down player
                //slowPlayer = true;
            }
        }
        else if (state == 2) {
            // SELF DEFENCE

            // Push player away, in direction of spawn (i.e. furthest distance)

            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 1 : 0;

            gameState.repelPlayer = true;

            currentSpeed = BIG_BOSS_RETREAT_SPEED;

            // Handle states
            if (!is_within_range(x, *playerX, BIG_BOSS_INJURED_MAX_RANGE) || !is_within_range(y, *playerY, BIG_BOSS_INJURED_MAX_RANGE)) {
                state = 3;
                minionsToSpawn = 1;
                gameState.repelPlayer = false;
            }

        }
        else if (state == 3) {
            // SPAWN MINIONS

            currentSpeed = 0;

            lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 0 : 1;

            // Jump whenever spawn minion
            if (!jumpCooldown && minionsToSpawn) {
                if (is_on_block()) {
                    shakeOnLanding = BIG_BOSS_ANGRY_JUMP_SHAKE_TIME;

                    // Spawn minion
                    gameState.enemies.push_back(Enemy(x + SPRITE_SIZE * 2, y + SPRITE_SIZE * 2, enemyHealths[bigBossMinions[health]], bigBossMinions[health]));
                    gameState.enemies[gameState.enemies.size() - 1].lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE * 2 ? 0 : 1;
                    gameState.enemies[gameState.enemies.size() - 1].set_player_position(playerX, playerY);
                    //enemies[enemies.size() - 1].set_speed(BIG_BOSS_MINION_SPEED - BIG_BOSS_MINION_SPEED_REDUCTION * (2 - health));
                    minionsToSpawn--;

                    jump(BIG_BOSS_ANGRY_JUMP_SPEED, BIG_BOSS_MINION_SPAWN_COOLDOWN);
                }
            }

            // Handle states
            if (!jumpCooldown && !minionsToSpawn) {
                if (health > 0) {
                    // Not dead
                    state = 1;
                    immuneTimer = 0;

                    // Unslow player
                    gameState.slowPlayer = false;
                }
                else {
                    // Dead
                    // Generate particles
                    particles = generate_particles(x + SPRITE_SIZE * 2, y + SPRITE_SIZE * 2, BOSS_DEATH_PARTICLE_GRAVITY_X, BOSS_DEATH_PARTICLE_GRAVITY_Y, bossDeathParticleColours[(uint8_t)enemyType], BOSS_DEATH_PARTICLE_SPEED, BOSS_DEATH_PARTICLE_COUNT);
                    deathParticles = true;
                    state = 4;
                    dead = true;
                    // Play death sfx
                    gameState.audioHandler.play(3);
                }
            }
        }
        else if (state == 4) {
            // Dead, displaying particles

            if (deathParticles) {
                if (particles.size() == 0) {
                    // No particles left
                    deathParticles = false;

                    // Unslow player
                    gameState.slowPlayer = false;
                }
                else {
                    for (Particle& particle : particles) {
                        particle.update(dt);
                    }

                    // Remove any particles which are too old
                    particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= BOSS_DEATH_PARTICLE_AGE); }), particles.end());
                }
            }
        }
    }

    if (y > gameState.levelDeathBoundary) {
        health = 0;
        xVel = yVel = 0;
    }
}

bool Boss::is_on_block() {
    if (is_big()) {
        // Allow boss to jump on tiles
        for (Tile& tile : gameState.foreground) {
            if (y + SPRITE_SIZE * 4 == tile.y && tile.x + SPRITE_SIZE - 1 > x && tile.x + 1 < x + SPRITE_SIZE * 4) {
                // On top of block
                return true;
            }
        }

        // Allow boss to jump on platforms
        for (Tile& platform : gameState.platforms) {
            if (y + SPRITE_SIZE * 4 == platform.y && platform.x + SPRITE_SIZE - 1 > x && platform.x + 1 < x + SPRITE_SIZE * 4) {
                // On top of block
                return true;
            }
        }
    }
    else {
        // Allow boss to jump on tiles
        for (Tile& tile : gameState.foreground) {
            if (y + SPRITE_SIZE * 2 == tile.y && tile.x + SPRITE_SIZE - 1 > x && tile.x + 1 < x + SPRITE_SIZE * 2) {
                // On top of block
                return true;
            }
        }

        // Allow boss to jump on platforms
        for (Tile& platform : gameState.platforms) {
            if (y + SPRITE_SIZE * 2 == platform.y && platform.x + SPRITE_SIZE - 1 > x && platform.x + 1 < x + SPRITE_SIZE * 2) {
                // On top of block
                return true;
            }
        }
    }

    // Boss didn't jump
    return false;

}

void Boss::set_immune() {
    immuneTimer = BOSS_IMMUNE_TIME;
    lastDirection = *playerX + SPRITE_HALF < x + SPRITE_SIZE ? 0 : 1;
}

bool Boss::is_immune() {
    if (enemyType == EnemyType::BASIC) {
        return Entity::is_immune() || state >= 2;
    }
    else if (enemyType == EnemyType::RANGED) {
        return Entity::is_immune() || state >= 2;
    }
    else if (enemyType == EnemyType::PURSUIT) {
        return Entity::is_immune() || state >= 2;
    }
    else {
        // Catch-all
        return false;
    }
}

void Boss::set_injured() {
    injuredTimer = BOSS_INJURED_TIME;
    set_immune();
}

uint8_t Boss::get_size() {
    if (is_big()) {
        return SPRITE_SIZE * 4;
    }
    else {
        return SPRITE_SIZE * 2;
    }
}

bool Boss::is_big() {
    return enemyType == EnemyType::PURSUIT;
}

bool Boss::is_within_range(float me, float them, float range) {
    //return get_range(me, them) < range;
    return get_abs_center_range(me, them) < range;
}

//float get_range(float me, float them) {
//    if (them > me) {
//        // They are right
//        return them - (me + SPRITE_SIZE * 2);
//    }
//    else {
//        // They are left
//        return me - (them + SPRITE_SIZE);
//    }
//}

float Boss::get_center_range(float me, float them) {
    return (me + SPRITE_SIZE) - (them + SPRITE_HALF);
}

float Boss::get_abs_center_range(float me, float them) {
    return std::abs(get_center_range(me, them));
}

void Boss::update_collisions() {
    if (!locked) {
        // Update gravity
        yVel += GRAVITY * gameState.dt;
        yVel = std::min(yVel, (float)GRAVITY_MAX);

        // Move entity y
        y += yVel * gameState.dt;

        // Here check collisions...
        for (Tile& tile : gameState.foreground) {
            if (colliding(tile)) {
                if (yVel > 0) {
                    // Collided from top
                    y = tile.y - SPRITE_SIZE * (is_big() ? 4 : 2);
                    if (shakeOnLanding) {
                        gameState.shaker.set_shake(shakeOnLanding);
                        shakeOnLanding = 0;
                        gameState.audioHandler.play(4);
                    }
                }
                else if (yVel < 0) {
                    // Collided from bottom
                    y = tile.y + SPRITE_SIZE;
                }
                yVel = 0;
            }
        }

        if (yVel != 0.0f) {
            // Platforms may need work
            if (!is_big()) {
                for (Tile& platform : gameState.platforms) {
                    if (colliding(platform)) {
                        if (yVel > 0 && y + SPRITE_SIZE * 2 < platform.y + SPRITE_QUARTER) {
                            // Collided from top
                            y = platform.y - SPRITE_SIZE * 2;
                            if (shakeOnLanding) {
                                gameState.shaker.set_shake(shakeOnLanding);
                                shakeOnLanding = 0;
                                gameState.audioHandler.play(4);
                            }
                            yVel = 0;
                        }
                    }
                }
            }
        }

        if (xVel != 0.0f) {
            // Move entity x
            x += xVel * gameState.dt;

            // Here check collisions...
            for (Tile& tile : gameState.foreground) {
                if (colliding(tile)) {
                    if (xVel > 0) {
                        // Collided from left
                        x = tile.x - SPRITE_SIZE * (is_big() ? 4 : 2) + 1;
                    }
                    else if (xVel < 0) {
                        // Collided from right
                        x = tile.x + SPRITE_SIZE - 1;
                    }
                    xVel = 0;
                }
            }

            if (xVel > 0) {
                lastDirection = 1;
            }
            else if (xVel < 0) {
                lastDirection = 0;
            }
        }
    }
}

void Boss::render(Camera& camera) {
    if (!dead) {
        if (is_big()) {
            uint16_t frame = anchorFrame;

            //if (true) { //state != 0 //health < 3
            //    frame = anchorFrame + 8 + 16 * 4;
            //}

            if (yVel < -50) {
                frame = anchorFrame + 4;
            }
            else if (yVel > 160) {
                frame = anchorFrame + 8;
            }

            if (injuredTimer) {
                frame = anchorFrame + 12;
            }

            if (lastDirection == 1) {
                for (uint8_t i = 0; i < 4; i++) {
                    for (uint8_t j = 0; j < 4; j++) {
                        render_sprite(frame + i + j * 16, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_SIZE * (3 - i), SCREEN_MID_HEIGHT + y - camera.y + SPRITE_SIZE * j), SpriteTransform::HORIZONTAL);
                    }
                }
            }
            else {
                for (uint8_t i = 0; i < 4; i++) {
                    for (uint8_t j = 0; j < 4; j++) {
                        render_sprite(frame + i + j * 16, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_SIZE * i, SCREEN_MID_HEIGHT + y - camera.y + SPRITE_SIZE * j));
                    }
                }
            }
        }
        else {
            uint16_t frame = anchorFrame;

            if (yVel < -50) {
                frame = anchorFrame + 2;
            }
            else if (yVel > 160) {
                frame = anchorFrame + 4;
            }

            if (injuredTimer) {
                frame = anchorFrame + 6;
            }

            if (lastDirection == 1) {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_SIZE, SCREEN_MID_HEIGHT + y - camera.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame + 1, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame + 16, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_SIZE, SCREEN_MID_HEIGHT + y - camera.y + SPRITE_SIZE), SpriteTransform::HORIZONTAL);
                render_sprite(frame + 17, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y + SPRITE_SIZE), SpriteTransform::HORIZONTAL);
            }
            else {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
                render_sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
                render_sprite(frame + 1, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_SIZE, SCREEN_MID_HEIGHT + y - camera.y));
                render_sprite(frame + 16, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y + SPRITE_SIZE));
                render_sprite(frame + 17, Point(SCREEN_MID_WIDTH + x - camera.x + SPRITE_SIZE, SCREEN_MID_HEIGHT + y - camera.y + SPRITE_SIZE));
            }
        }
    }

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}

bool Boss::colliding(Tile& tile) {
    if (is_big()) {
        return (tile.x + SPRITE_SIZE > x + 1 && tile.x < x + SPRITE_SIZE * 4 - 1 && tile.y + SPRITE_SIZE > y && tile.y < y + SPRITE_SIZE * 4);
    }
    else {
        return (tile.x + SPRITE_SIZE > x + 1 && tile.x < x + SPRITE_SIZE * 2 - 1 && tile.y + SPRITE_SIZE > y && tile.y < y + SPRITE_SIZE * 2);
    }
}

bool Boss::spawning_minions() {
    return minionsToSpawn;
}

bool Boss::is_dead() {
    return dead;
}

bool Boss::particles_left() {
    return deathParticles;
}

void Boss::reset() {
    dead = false;
    particles.clear();
    shakeOnLanding = false;
    x = spawnX;
    y = spawnY;
    injuredTimer = 0;
    minionsToSpawn = 0;
    state = 0;
    health = bossHealths[(uint8_t)enemyType];
}