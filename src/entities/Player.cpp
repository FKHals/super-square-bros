#include "Entity.hpp"

#include "../scenes/Scene.h"
#include "../GameState.hpp"


using namespace blit;


const uint8_t PLAYER_START_LIVES = 3;

const float PLAYER_MAX_JUMP = 190.0f;
//const float  PLAYER_ATTACK_JUMP = 90.0f;
const float PLAYER_ATTACK_JUMP_SCALE = 0.65f;
const float PLAYER_ATTACK_JUMP_MIN = 90.0f;
const float PLAYER_MAX_SPEED = 87.0f;
const float PLAYER_ACCELERATION = 370.0f;
const float PLAYER_DECELERATION = 330.0f;
const float PLAYER_JUMP_COOLDOWN = 0.4f;
const float PLAYER_JUMP_MIN_AIR_TIME = 0.05f;
//const float PLAYER_JUMP_EXTRA_GRAVITY = 500.0f;
const float PLAYER_JUMP_EXTRA_DOWNSCALE = 0.92f;

const float PLAYER_SLOW_MAX_JUMP = 110.0f;
const float PLAYER_SLOW_MAX_SPEED = 15.0f;
const float PLAYER_SLOW_ACCELERATION = 150.0f;
const float PLAYER_SLOW_DECELERATION = 100.0f;
const float PLAYER_SLOW_JUMP_COOLDOWN = 0.3f;

const float PLAYER_SLOW_PARTICLE_GRAVITY_X = 0.0f;
const float PLAYER_SLOW_PARTICLE_GRAVITY_Y = -30.0f;
const float PLAYER_SLOW_PARTICLE_SPEED = 50.0f;
const uint8_t PLAYER_SLOW_PARTICLE_WIGGLE = 1;
const float PLAYER_SLOW_PARTICLE_SPAWN_DELAY = 0.05f;


Player::Player() : Entity() {
    score = 0;
    enemiesKilled = 0;
    levelTimer = 0.0f;

    id = 0;

    lives = PLAYER_START_LIVES;

    slowPlayerParticleTimer = 0.0f;
    airTime = 0.0f;
}

Player::Player(uint16_t xPosition, uint16_t yPosition, uint8_t colour) : Entity(xPosition, yPosition, TILE_ID_PLAYER_1 + colour * 4, PLAYER_MAX_HEALTH) {
    score = 0;
    enemiesKilled = 0;
    levelTimer = 0.0f;

    id = colour;

    lives = PLAYER_START_LIVES;

    slowPlayerParticleTimer = 0.0f;
    airTime = 0.0f;
}

void Player::update(float dt, ButtonStates& buttonStates) {
    if (immuneTimer) {
        immuneTimer -= dt;
        if (immuneTimer < 0) {
            immuneTimer = 0;
        }
    }

    if (jumpCooldown) {
        jumpCooldown -= dt;
        if (jumpCooldown < 0) {
            jumpCooldown = 0;
        }
    }

    if (health > 0) {

#ifdef TESTING_MODE
        health = 3;

        if (buttonStates.X) {
            x = finish.x - SPRITE_SIZE * 2;
            y = finish.y;
        }
#endif // TESTING_MODE

        if (!locked) {
            levelTimer += dt;

            if (is_on_block()) {
                airTime = 0.0f;
            }
            else {
                airTime += dt;
            }

            if (buttonStates.A && jumpCooldown == 0 && airTime < PLAYER_JUMP_MIN_AIR_TIME) {
                jump(gameState.slowPlayer ? PLAYER_SLOW_MAX_JUMP : PLAYER_MAX_JUMP, gameState.slowPlayer ? PLAYER_SLOW_JUMP_COOLDOWN : PLAYER_JUMP_COOLDOWN);
                if (!gameState.currentScene->instanceOf(&scene::characterSelect)) {
                    // Play jump sfx
                    gameState.audioHandler.play(1);
                }
            }
            else if (!buttonStates.A && yVel < 0) {
                yVel *= PLAYER_JUMP_EXTRA_DOWNSCALE;
            }

            if (buttonStates.LEFT) {
                if (gameState.slowPlayer) {
                    xVel -= PLAYER_SLOW_ACCELERATION * dt;
                    if (xVel < -PLAYER_SLOW_MAX_SPEED) {
                        xVel = -PLAYER_SLOW_MAX_SPEED;
                    }
                }
                else {
                    xVel -= PLAYER_ACCELERATION * dt;
                    if (xVel < -PLAYER_MAX_SPEED) {
                        xVel = -PLAYER_MAX_SPEED;
                    }
                }
            }
            else if (buttonStates.RIGHT) {
                if (gameState.slowPlayer) {
                    xVel += PLAYER_SLOW_ACCELERATION * dt;
                    if (xVel > PLAYER_SLOW_MAX_SPEED) {
                        xVel = PLAYER_SLOW_MAX_SPEED;
                    }
                }
                else {
                    xVel += PLAYER_ACCELERATION * dt;
                    if (xVel > PLAYER_MAX_SPEED) {
                        xVel = PLAYER_MAX_SPEED;
                    }
                }
            }
            else {
                if (xVel > 0) {
                    if (gameState.slowPlayer) {
                        xVel -= PLAYER_SLOW_DECELERATION * dt;
                    }
                    else {
                        xVel -= PLAYER_DECELERATION * dt;
                    }

                    if (xVel < 0) {
                        xVel = 0;
                    }
                }
                else if (xVel < 0) {
                    if (gameState.slowPlayer) {
                        xVel += PLAYER_SLOW_DECELERATION * dt;
                    }
                    else {
                        xVel += PLAYER_DECELERATION * dt;
                    }

                    if (xVel > 0) {
                        xVel = 0;
                    }
                }
            }
        }

        uint8_t coinCount = gameState.coins.size();

        // Remove coins if player jumps on them
        gameState.coins.erase(std::remove_if(gameState.coins.begin(), gameState.coins.end(),
                                               [this](Coin& coin) { return (coin.x + SPRITE_SIZE > x&& coin.x < x + SPRITE_SIZE && coin.y + SPRITE_SIZE > y && coin.y < y + SPRITE_SIZE); }),
                                gameState.coins.end());

        // Add points to player score (1 point per coin which has been deleted)
        score += coinCount - gameState.coins.size();

        if (coinCount != gameState.coins.size()) {
            // Must have picked up a coin
            // Play coin sfx
            gameState.audioHandler.play(gameState.coinSfxAlternator ? 0 : 2);
            gameState.coinSfxAlternator = !gameState.coinSfxAlternator;
        }


        uint8_t enemyCount = gameState.enemies.size();// + bosses.size();

        // Remove enemies if no health left
        gameState.enemies.erase(std::remove_if(gameState.enemies.begin(), gameState.enemies.end(),
                                                 [](Enemy& enemy) { return (enemy.health == 0 && enemy.particles.size() == 0); }),
                                  gameState.enemies.end());
        gameState.bosses.erase(std::remove_if(gameState.bosses.begin(), gameState.bosses.end(),
                                                [](Boss& boss) { return (boss.is_dead() && !boss.particles_left()); }),
                                 gameState.bosses.end());

        enemiesKilled += enemyCount - gameState.enemies.size();// - bosses.size();

        update_collisions();


        if (!is_immune()) {
            for (Tile& spike : gameState.spikes) {
                if (Entity::colliding(spike)) {
                    if ((spike.get_id() == TILE_ID_SPIKE_BOTTOM && y + SPRITE_SIZE >= spike.y + SPRITE_HALF) ||
                        (spike.get_id() == TILE_ID_SPIKE_TOP && y <= spike.y + SPRITE_HALF) ||
                        (spike.get_id() == TILE_ID_SPIKE_LEFT && x <= spike.x + SPRITE_HALF) ||
                        (spike.get_id() == TILE_ID_SPIKE_RIGHT && x + SPRITE_SIZE >= spike.x + SPRITE_HALF)) {

                        health -= 1;
                        set_immune();
                        break;
                    }
                }
            }
        }

        if (y > gameState.levelDeathBoundary) {
            health = 0;
            xVel = yVel = 0;
        }
    }

    if (gameState.slowPlayer || gameState.repelPlayer) {
        slowPlayerParticleTimer += dt;
        if (slowPlayerParticleTimer >= PLAYER_SLOW_PARTICLE_SPAWN_DELAY) {
            slowPlayerParticleTimer -= PLAYER_SLOW_PARTICLE_SPAWN_DELAY;
            slowParticles.push_back(generate_brownian_particle(x + SPRITE_HALF, y + SPRITE_HALF, PLAYER_SLOW_PARTICLE_GRAVITY_X, PLAYER_SLOW_PARTICLE_GRAVITY_Y, PLAYER_SLOW_PARTICLE_SPEED, gameState.repelPlayer ? repelPlayerParticleColours : slowPlayerParticleColours, PLAYER_SLOW_PARTICLE_WIGGLE));
        }
    }
    else {
        slowPlayerParticleTimer = 0.0f;
    }

    for (BrownianParticle& slowParticle : slowParticles) {
        slowParticle.update(dt);
    }
    // Remove any particles which are too old
    slowParticles.erase(std::remove_if(slowParticles.begin(), slowParticles.end(), [](BrownianParticle& particle) { return (particle.age >= PLAYER_SLOW_PARTICLE_AGE); }), slowParticles.end());



    if (health == 0) {
        //state = DEAD;

        if (deathParticles) {
            if (particles.size() == 0) {
                // No particles left, reset values which need to be

                deathParticles = false;

                // If player has lives left, respawn
                if (lives) {
                    // Reset player position and health, maybe remove all this?
                    health = 3;
                    xVel = yVel = 0;
                    lastDirection = 1;

                    // Go to checkpoint if checkpoints are enabled, and have reached it.
                    if (gameState.gameSaveData.checkpoints && gameState.checkpoint.colour) {
                        x = gameState.checkpoint.x;
                        y = gameState.checkpoint.y;
                    }
                    else {
                        x = gameState.playerStartX;
                        y = gameState.playerStartY;
                    }

                    // Stop player from moving while respawning
                    gameState.cameraRespawn = true;
                    locked = true;
                    gameState.slowPlayer = false;
                    gameState.dropPlayer = false;
                    gameState.repelPlayer = false;
                    gameState.bossBattle = false;

                    // reset bosses
                    for (Boss &boss : gameState.bosses) {
                        boss.reset();
                    }

                    // Make player immune when respawning?
                    //set_immune();

                    // Remove immunity when respawning
                    immuneTimer = 0;
                }
            }
            else {
                for (Particle& particle : particles) {
                    particle.update(dt);
                }

                // Remove any particles which are too old
                particles.erase(std::remove_if(particles.begin(), particles.end(), [](Particle& particle) { return (particle.age >= ENTITY_DEATH_PARTICLE_AGE); }), particles.end());
            }
        }
        else if (lives) {
            // Generate particles
            particles = generate_particles(x + SPRITE_HALF, y + SPRITE_HALF, ENTITY_DEATH_PARTICLE_GRAVITY_X, ENTITY_DEATH_PARTICLE_GRAVITY_Y, playerDeathParticleColours[id], ENTITY_DEATH_PARTICLE_SPEED, ENTITY_DEATH_PARTICLE_COUNT);
            deathParticles = true;

            // Reduce player lives by one
            lives--;

            gameState.audioHandler.play(5);
        }
    }
}

void Player::update_collisions() {
    if (!locked) {
        // Update gravity
        yVel += GRAVITY * gameState.dt;
        yVel = std::min(yVel, GRAVITY_MAX);

        // Move entity y
        y += yVel * gameState.dt;

        // Here check collisions...

        // Enemies first
        for (Enemy& enemy : gameState.enemies) {
            if (enemy.health && colliding(enemy)) {
                if (y + SPRITE_SIZE < enemy.y + SPRITE_QUARTER) {
                    // Collided from top
                    y = enemy.y - SPRITE_SIZE;

                    if (yVel > 0.0f || enemy.yVel < 0) { // && !enemies[i].is_immune()
                        //yVel = -PLAYER_ATTACK_JUMP;
                        yVel = -std::max(yVel * PLAYER_ATTACK_JUMP_SCALE, PLAYER_ATTACK_JUMP_MIN);

                        // Take health off enemy
                        enemy.health--;

                        // Play enemy injured sfx
                        if (enemy.health) {
                            gameState.audioHandler.play(4);
                        }

                        if (enemy.yVel < 0) {
                            // Enemy is jumping
                            // Stop enemy's jump
                            enemy.yVel = 0;
                        }
                    }
                }
            }
        }

        if (yVel > 0.0f) {
            if (!gameState.dropPlayer) {
                for (Boss& boss : gameState.bosses) {
                    if (!boss.is_dead() && colliding(boss)) {
                        if (y + SPRITE_SIZE < boss.y + SPRITE_QUARTER) {
                            // Collided from top
                            y = boss.y - SPRITE_SIZE;

                            if (!boss.is_immune() && boss.health) {
                                //yVel = -PLAYER_ATTACK_JUMP;
                                yVel = -std::max(yVel * PLAYER_ATTACK_JUMP_SCALE, PLAYER_ATTACK_JUMP_MIN);

                                // Take health off enemy
                                boss.health--;

                                // Play enemy injured sfx
                                if (boss.health) {
                                    gameState.audioHandler.play(4);
                                }

                                //if (boss.yVel < 0) {
                                // Enemy is jumping
                                // Stop enemy's jump
                                //bosses[i].yVel = 0;
                                //}

                                boss.set_injured();
                            }
                        }
                    }
                }
            }
        }

        for (Tile& tile : gameState.foreground) {
            if (Entity::colliding(tile)) {
                if (yVel > 0 && y + SPRITE_SIZE < tile.y + SPRITE_HALF) {
                    // Collided from top
                    y = tile.y - SPRITE_SIZE;
                    gameState.dropPlayer = false; // stop player falling through platforms (only used in boss #2 currently)
                }
                else if (yVel < 0 && y + SPRITE_SIZE > tile.y + SPRITE_HALF) {
                    // Collided from bottom
                    y = tile.y + SPRITE_SIZE;
                }
                yVel = 0;
                break;
            }
        }


        if (yVel != 0.0f) {
            // Platforms may need work
            if (!gameState.dropPlayer) {
                for (Tile& platform : gameState.platforms) {
                    handle_platform_collisions(platform);
                }
            }

            for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
                if (levelTrigger.visible && colliding(levelTrigger)) {
                    if (yVel > 0 && y + SPRITE_SIZE < levelTrigger.y + SPRITE_HALF) {
                        if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached >= levelTrigger.levelNumber) {
                            // Level is unlocked

                            // Collided from top
                            y = levelTrigger.y - SPRITE_SIZE;
                            //yVel = -PLAYER_ATTACK_JUMP;
                            yVel = -std::max(yVel * PLAYER_ATTACK_JUMP_SCALE, PLAYER_ATTACK_JUMP_MIN);

                            levelTrigger.set_active();

                            // Play sfx
                            gameState.audioHandler.play(4);
                        }
                        else {
                            // Level is locked, act as a solid object

                            // Collided from top
                            y = levelTrigger.y - SPRITE_SIZE;
                            yVel = 0;
                        }
                    }
                }
            }
        }

        // Move entity x
        if (xVel != 0.0f) {
            x += xVel * gameState.dt;

            // Here check collisions...
            for (Tile& tile : gameState.foreground) {
                if (Entity::colliding(tile)) {
                    if (xVel > 0) {
                        // Collided from left
                        x = tile.x - SPRITE_SIZE + 1;
                    }
                    else if (xVel < 0) {
                        // Collided from right
                        x = tile.x + SPRITE_SIZE - 1;
                    }
                    xVel = 0;
                    break;
                }
            }

            for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
                if (levelTrigger.visible && colliding(levelTrigger)) {
                    if (xVel > 0) {
                        // Collided from left
                        x = levelTrigger.x - SPRITE_SIZE;
                    }
                    else if (xVel < 0) {
                        // Collided from right
                        x = levelTrigger.x + SPRITE_SIZE;
                    }
                    xVel = 0;
                    break;
                }
            }

            if (xVel > 0) {
                lastDirection = 1;
            }
            else if (xVel < 0) {
                lastDirection = 0;
            }
        }

        if (!immuneTimer && !gameState.dropPlayer) {
            for (Enemy& enemy : gameState.enemies) {
                if (colliding(enemy) && enemy.health) {
                    health--;
                    set_immune();
                }
            }

            for (Boss& boss : gameState.bosses) {
                if (colliding(boss) && boss.health) {
                    health--;
                    set_immune();
                }
            }
        }

        if (colliding(gameState.checkpoint)) {
            if (gameState.checkpoint.activate(id + 1)) {
                gameState.audioHandler.play(4); // need to load + play activate sound - swap with playerdeath
            }
        }

        if (x < -SCREEN_MID_WIDTH) {
            x = -SCREEN_MID_WIDTH;
        }
        else if (x > gameState.levelData.levelWidth * SPRITE_SIZE + SCREEN_MID_WIDTH) {
            x = gameState.levelData.levelWidth * SPRITE_SIZE + SCREEN_MID_WIDTH;
        }
    }
}

void Player::render(Camera& camera) {
    // Particles
    for (BrownianParticle& slowParticle : slowParticles) {
        slowParticle.render(camera);
    }

    if (health != 0) {
        bool visible = false;

        if (immuneTimer) {
            uint16_t immuneTimer_ms = (uint16_t)(immuneTimer * 1000);
            if (immuneTimer_ms % 150 < 75) {
                visible = true;
            }
        }
        else {
            visible = true;
        }

        if (visible) {
            uint16_t frame = anchorFrame;

            if (yVel < -50) {
                frame = anchorFrame + 1;
            }
            else if (yVel > 160) {
                frame = anchorFrame + 2;
            }

            if (lastDirection == 1) {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y), SpriteTransform::HORIZONTAL);
            }
            else {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
                render_sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
            }


            if (gameState.slowPlayer || gameState.repelPlayer) {
                render_sprite(448, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
            }
        }
    }

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}

//using Entity::colliding;

bool Player::colliding(Enemy& enemy) {
    // Replace use of this with actual code?
    return (enemy.x + SPRITE_SIZE > x && enemy.x < x + SPRITE_SIZE && enemy.y + SPRITE_SIZE > y && enemy.y < y + SPRITE_SIZE);
}

bool Player::colliding(Boss& boss) {
    if (boss.is_big()) {
        return (boss.x + SPRITE_SIZE * 4 > x && boss.x < x + SPRITE_SIZE && boss.y + SPRITE_SIZE * 4 > y && boss.y < y + SPRITE_SIZE);
    }
    else {
        return (boss.x + SPRITE_SIZE * 2 > x && boss.x < x + SPRITE_SIZE && boss.y + SPRITE_SIZE * 2 > y && boss.y < y + SPRITE_SIZE);
    }
}

bool Player::colliding(LevelTrigger& levelTrigger) {
    // Replace use of this with actual code?
    return (levelTrigger.x + SPRITE_SIZE > x && levelTrigger.x < x + SPRITE_SIZE && levelTrigger.y + SPRITE_SIZE > y && levelTrigger.y < y + SPRITE_SIZE);
}

bool Player::colliding(Checkpoint& c) {
    return (c.x + SPRITE_SIZE > x && c.x < x + SPRITE_SIZE && c.y + SPRITE_SIZE > y && c.y - SPRITE_SIZE < y + SPRITE_SIZE);
}
