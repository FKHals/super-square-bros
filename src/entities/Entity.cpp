#include "Entity.hpp"

#include "../GameState.hpp"


const float PLAYER_IMMUNE_TIME = 2.0f;

Entity::Entity() {
    x = y = 0;
    xVel = yVel = 0;

    anchorFrame = 0;

    lastDirection = 1; // 1 = right, 0 = left
    //state = IDLE;
    locked = false;
    deathParticles = false;

    health = 1;

    immuneTimer = 0;
    jumpCooldown = 0;
}

Entity::Entity(uint16_t xPosition, uint16_t yPosition, uint16_t frame, uint8_t startHealth) {
    x = xPosition;
    y = yPosition;
    xVel = yVel = 0;

    anchorFrame = frame;

    lastDirection = 1;
    //state = IDLE;
    locked = false;
    deathParticles = false;

    health = startHealth;

    immuneTimer = 0;
    jumpCooldown = 0;
}

void Entity::update(float dt, ButtonStates& buttonStates) {

}

void Entity::update_collisions() {
    if (!locked) {
        // Update gravity
        yVel += GRAVITY * gameState.dt;
        yVel = std::min(yVel, (float)GRAVITY_MAX);

        // Move entity y
        y += yVel * gameState.dt;

        // Here check collisions...
        for (Tile& tile : gameState.foreground) {
            if (colliding(tile)) {
                if (yVel > 0) {
                    // Collided from top
                    y = tile.y - SPRITE_SIZE;
                }
                else if (yVel < 0) {
                    // Collided from bottom
                    y = tile.y + SPRITE_SIZE;
                }
                yVel = 0;
                break;
            }
        }


        if (yVel != 0.0f) {
            // Platforms may need work
            for (Tile& platform : gameState.platforms) {
                if (handle_platform_collisions(platform)) {
                    break;
                }
            }
        }

        if (xVel != 0.0f) {
            // Move entity x
            x += xVel * gameState.dt;

            // Here check collisions...
            for (Tile& tile : gameState.foreground) {
                if (colliding(tile)) {
                    if (xVel > 0) {
                        // Collided from left
                        x = tile.x - SPRITE_SIZE + 1;
                    }
                    else if (xVel < 0) {
                        // Collided from right
                        x = tile.x + SPRITE_SIZE - 1;
                    }
                    xVel = 0;
                    break;
                }
            }

            if (xVel > 0) {
                lastDirection = 1;
            }
            else if (xVel < 0) {
                lastDirection = 0;
            }
        }
    }
}

void Entity::jump(float jumpVel, float cooldown) {
    // Jump
    yVel = -jumpVel;
    jumpCooldown = cooldown;
}

bool Entity::is_on_block() {
    // Is entity on a tile?
    for (Tile& tile : gameState.foreground) {
        if (y + SPRITE_SIZE == tile.y && tile.x + SPRITE_SIZE - 1 > x && tile.x + 1 < x + SPRITE_SIZE) {
            // On top of block
            return true;
        }
    }

    // Is entity on a platform?
    for (Tile& platform : gameState.platforms) {
        if (y + SPRITE_SIZE == platform.y && platform.x + SPRITE_SIZE - 1 > x && platform.x + 1 < x + SPRITE_SIZE) {
            // On top of block
            return true;
        }
    }

    // Is entity on a locked LevelTrigger?
    for (LevelTrigger& levelTrigger : gameState.levelTriggers) {
        if (y + SPRITE_SIZE == levelTrigger.y && levelTrigger.x + SPRITE_SIZE - 1 > x && levelTrigger.x + 1 < x + SPRITE_SIZE) {
            // On top of block
            if (gameState.allPlayerSaveData[gameState.playerSelected].levelReached < levelTrigger.levelNumber) {
                // LevelTrigger is locked
                return true;
            }
        }
    }

    // Not on a block
    return false;
}

bool Entity::handle_platform_collisions(Tile& platform) {
    if (colliding(platform)) {
        if (yVel > 0 && y + SPRITE_SIZE < platform.y + SPRITE_QUARTER) {
            // Collided from top
            y = platform.y - SPRITE_SIZE;
            yVel = 0;
            return true;
        }
    }

    return false;
}

void Entity::render(Camera& camera) {
    if (health != 0) {
        bool visible = false;

        if (immuneTimer) {
            uint16_t immuneTimer_ms = (uint16_t)(immuneTimer * 1000);
            if (immuneTimer_ms % 150 < 75) {
                visible = true;
            }
        }
        else {
            visible = true;
        }

        if (visible) {
            uint16_t frame = anchorFrame;

            if (yVel < -50) {
                frame = anchorFrame + 1;
            }
            else if (yVel > 160) {
                frame = anchorFrame + 2;
            }

            /*if (immuneTimer) {
                frame = anchorFrame + 3;
            }*/

            if (lastDirection == 1) {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y), SpriteTransform::HORIZONTAL);
                render_sprite(frame, blit::Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y), blit::HORIZONTAL);
            }
            else {
                //screen.sprite(frame, Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
                render_sprite(frame, blit::Point(SCREEN_MID_WIDTH + x - camera.x, SCREEN_MID_HEIGHT + y - camera.y));
            }
        }
    }

    // Particles
    for (Particle& particle : particles) {
        particle.render(camera);
    }
}

bool Entity::colliding(Tile& tile) {
#ifdef PICO_BUILD
    // 24.8 fixed-point, thanks to Daft Freak
    const int scale = 256;
    int ix = int(x * scale);
    int iy = int(y * scale);

    return ((tile.x + SPRITE_SIZE) * scale > ix + scale
        && tile.x * scale < ix + (SPRITE_SIZE - 1) * scale
        && (tile.y + SPRITE_SIZE) * scale > iy
        && tile.y * scale < iy + SPRITE_SIZE * scale);
#else
    return (tile.x + SPRITE_SIZE > x + 1 && tile.x < x + SPRITE_SIZE - 1 && tile.y + SPRITE_SIZE > y && tile.y < y + SPRITE_SIZE);
#endif // PICO_BUILD
}

void Entity::set_immune() {
    immuneTimer = PLAYER_IMMUNE_TIME;
}

bool Entity::is_immune() {
    return immuneTimer;
}
