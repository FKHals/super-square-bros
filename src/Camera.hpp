#ifndef SUPER_SQUARE_BROS_CAMERA_HPP
#define SUPER_SQUARE_BROS_CAMERA_HPP

const float CAMERA_SCALE_X = 10.0f;
const float CAMERA_SCALE_Y = 5.0f;

class Camera {
public:
    float x, y;
    float tempX, tempY;

    bool locked;

    Camera();
    void reset_temp();
    void reset_timer();
    bool timer_started();
    void start_timer();
    void update_timer(float dt);
    float get_timer();
    void ease_out_to(float dt, float targetX, float targetY);
    void linear_to(float dt, float startX, float startY, float targetX, float targetY, float time);
protected:
    float timer;
};


const float SCREEN_SHAKE_SHAKINESS = 3.0f; //pixels either way - maybe more?

class ScreenShake {
public:
    ScreenShake();
    ScreenShake(float shakiness);
    void set_shake(float shake);
    void set_shakiness(float shakiness);
    float time_to_shake(float dt);

protected:
    float shake;
    float shakiness;
};

#endif //SUPER_SQUARE_BROS_CAMERA_HPP
