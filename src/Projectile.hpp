#ifndef SUPER_SQUARE_BROS_PROJECTILE_H
#define SUPER_SQUARE_BROS_PROJECTILE_H

#include "Camera.hpp"
#include "GameInput.hpp"
#include "Constants.hpp"

const float PROJECTILE_GRAVITY = 55.0f;
const float PROJECTILE_GRAVITY_MAX = 100.0f;

class Projectile {
public:
    float x, y;
    float xVel, yVel;
    uint16_t id;
    uint8_t width;

    Projectile();
    Projectile(float xPosition, float yPosition,
               float xVelocity, float yVelocity,
               uint16_t tileId, bool gravity=true, uint8_t rectWidth=SPRITE_HALF);
    void update(float dt, ButtonStates& buttonStates);
    void render(Camera& camera);
    bool is_colliding(float playerX, float playerY);

protected:
    bool gravity;
};

#endif //SUPER_SQUARE_BROS_PROJECTILE_H
